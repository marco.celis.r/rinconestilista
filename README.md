# Rincon Estilista

> Taller de Integracion de Software
> Version final del software

Esta version cuenta con las siguientes funcionalidades:
```
- Registro de usuarios
- Login con credenciales
- 3 perfiles de usuarios
- Reserva de horas
- Recepcion de reservas
- Anulacion de reservas
- Creacion de profesionales
- Creacion de servicios
- Registro de promociones empresas
- Despliegue aleatorio de promociones
```

Funcionalidades a futuro
```
- Todos los mantenedores
- Publicidad por pago y/o mejores ofertas
```

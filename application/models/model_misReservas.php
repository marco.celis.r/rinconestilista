<?php
  class Model_misReservas extends CI_Model
  {
    function insertar($data)
    {
      $this->db->insert('servicio', $data);
    }

    function GetAll()
    {
      $query = $this->db->get('servicio');
      return $query->result();
    }

    function verificar($data)
    {
      $query = $this->db->get('servicio');
      return $query->result();
    }


    public function misReserva($usuario)
    {
      $sSql = "select re.idReserva, date_format(re.fecha, '%d-%m-%Y')  fecha ";
      $sSql = $sSql." , re.hora, pro.nombre nombreP, ser.descripcion ";
      $sSql = $sSql." , usu.nombre nombreU, SUBSTRING_INDEX(SUBSTRING_INDEX(re.estado,'|',2),'|',-1) estado ";
      $sSql = $sSql." , date_format(now(), '%Y-%m-%d') hoy, re.fecha fechaIngles";
      $sSql = $sSql." from reserva re ";
      $sSql = $sSql." , profesional pro ";
      $sSql = $sSql." , servicio ser ";
      $sSql = $sSql." , usuario usu ";
      $sSql = $sSql." where re.idEmpresa =  ".$usuario;
      $sSql = $sSql." and re.fecha >= date(now()) ";
      $sSql = $sSql." and upper(SUBSTRING_INDEX(SUBSTRING_INDEX(re.estado,'|',2),'|',-1)) in ('VIGENTE', 'RECEPCIONADO') ";
      $sSql = $sSql." and pro.idProfesional = re.idProfesional ";
      $sSql = $sSql." and pro.vigencia = 'S' ";
      $sSql = $sSql." and ser.idServicio = re.idServicio ";
      $sSql = $sSql." and ser.vigencia = 'S' ";
      $sSql = $sSql." and usu.idUsuario = re.idUsuario ";
      $sSql = $sSql." and usu.vigencia = 'S' ";
      $sSql = $sSql." order by  re.fecha, re.hora";


      $query = $this->db->query($sSql);
      return $query;
    }

    public function anulaReservaProfesional($idReserva, $idUsuario)
    {
      $sSql = " update reserva ";
      $sSql = $sSql." set estado = concat(idReserva,'|Anulado|usuario|".$idUsuario."|',fecha)";
      $sSql = $sSql." where idReserva = ".$idReserva;

      if(!$this->db->simple_query($sSql))
      {
        return FALSE;
      }
      return TRUE;
    }

    public function recepcionaReservaProfesional($idReserva, $idUsuario)
    {
      $sSql = " update reserva ";
      $sSql = $sSql." set estado = concat(idReserva,'|Recepcionado|usuario|".$idUsuario."|',fecha)";
      $sSql = $sSql." where idReserva = ".$idReserva;

      if(!$this->db->simple_query($sSql))
      {
        return FALSE;
      }
      return TRUE;
    }

    public function datosReserva($idReserva)
    {
      $sSql = "select re.idReserva, date_format(re.fecha, '%d-%m-%Y') fecha ";
      $sSql = $sSql." , re.hora, pro.nombre nombreP, ser.descripcion, usu.nombre nombreU ";
      $sSql = $sSql." , pro.correo correoP, usu.correo correU";
      $sSql = $sSql." , pro.idProfesional, usu.idUsuario";
      $sSql = $sSql." from reserva re ";
      $sSql = $sSql." , profesional pro ";
      $sSql = $sSql." , servicio ser ";
      $sSql = $sSql." , usuario usu ";
      $sSql = $sSql." where re.idReserva =  ".$idReserva;
      $sSql = $sSql." and pro.idProfesional = re.idProfesional ";
      $sSql = $sSql." and pro.vigencia = 'S' ";
      $sSql = $sSql." and ser.idServicio = re.idServicio ";
      $sSql = $sSql." and ser.vigencia = 'S' ";
      $sSql = $sSql." and usu.idUsuario = re.idUsuario ";
      $sSql = $sSql." and usu.vigencia = 'S' ";
      $sSql = $sSql." order by  re.fecha, re.hora";


      $query = $this->db->query($sSql);
      return $query;
    }

    public function anulaReservaUsuario($idReserva, $idUsuario)
    {
      $sSql = " update reserva ";
      $sSql = $sSql." set estado = concat(idReserva,'|Anulado|Profesional|".$idUsuario."|',fecha)";
      $sSql = $sSql." where idReserva = ".$idReserva;

      if(!$this->db->simple_query($sSql))
      {
        return FALSE;
      }
      return TRUE;
    }
  }
?>

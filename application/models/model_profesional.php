<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  class Model_Profesional extends CI_Model
  {
    function insertar($data)
    {
      $this->db->insert('profesional', $data);
    }

    function GetAll()
    {
      $query = $this->db->get('profesional');
      return $query->result();
    }

    function verificar($data)
    {
      $query = $this->db->get('profesional');
      return $query->result();
    }

    function existeProfesional($correo, $nombre)
    {
      $sSql = "";
      $sSql = "select count(1) existe ";
      $sSql = $sSql." from profesional";
      $sSql = $sSql." where (upper(correo) = upper('".$correo."') or upper(nombre) = upper('".$nombre."'))";
      $sSql = $sSql." and vigencia = 'S'";

      $query = $this->db->query($sSql);

      if ($query->num_rows() > 0)
      {
         $row = $query->row();
         $contador = intval($row->existe);

         if ($contador > 0)
         {
           return TRUE;
         }
      }
      return FALSE;

    }

  }
?>

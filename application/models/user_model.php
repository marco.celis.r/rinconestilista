<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {

  function getUsers($postData,$empresa,$sucursal)
  {
    $response = array();
    $this->db->select('*');

    if($postData['search'] )
    {
      $this->db->where("nombre like '%".$postData['search']."%' ");
      $this->db->where('idEmpresa', $empresa);
      $this->db->where('idSucursal', $sucursal);
      $records = $this->db->get('profesional')->result();
      foreach($records as $row )
      {
        $response[] = array("value"=>$row->idProfesional,"label"=>$row->nombre);
      }
    }

    return $response;
  }

  function getService($postData,$empresa,$sucursal)
  {
    $response = array();
    $this->db->select('*');

    if($postData['search'] )
    {
      $this->db->where("nombre like '%".$postData['search']."%' ");
      $this->db->where('idEmpresa', $empresa);
      $this->db->where('idSucursal', $sucursal);
      $records = $this->db->get('servicio')->result();
      foreach($records as $row )
      {
        $response[] = array("value"=>$row->idServicio,"label"=>$row->nombre);
      }
    }

    return $response;
  }

  function getAgenda($frecuencia, $fecha,$horaIni, $horaFin)
  {
    $sql = "select horasub from (  ";
    $sql = $sql." select fecha , time((DATE_SUB(fecha, INTERVAL '".$frecuencia.":0' MINUTE_SECOND))) horasub  ";
    $sql = $sql." from (SELECT STR_TO_DATE('".$fecha." 00:00:00','%Y-%m-%d %H:%i:%s') + INTERVAL ".$frecuencia." * SEQ.row MINUTE AS Fecha ";
    $sql = $sql." FROM (SELECT  @row := @row + 1 as row  ";
    $sql = $sql." FROM  (select 0 union all select 1 union all select 3 union all select 4 union all select 5 union all select 6 union all select 6 union all select 7 union all select 8 union all select 9) t  ";
    $sql = $sql." CROSS JOIN (select 0 union all select 1 union all select 3 union all select 4 union all select 5 union all select 6 union all select 6 union all select 7 union all select 8 union all select 9) t2   ";
    $sql = $sql." CROSS JOIN (select 0 union all select 1 union all select 3 union all select 4 union all select 5 union all select 6 union all select 6 union all select 7 union all select 8 union all select 9) t3   ";
    $sql = $sql." CROSS JOIN (select 0 union all select 1 union all select 3 union all select 4 union all select 5 union all select 6 union all select 6 union all select 7 union all select 8 union all select 9) t4   ";
    $sql = $sql." CROSS JOIN (select 0 union all select 1 union all select 3 union all select 4 union all select 5 union all select 6 union all select 6 union all select 7 union all select 8 union all select 9) t5   ";
    $sql = $sql." CROSS JOIN (select 0 union all select 1 union all select 3 union all select 4 union all select 5 union all select 6 union all select 6 union all select 7 union all select 8 union all select 9) t6   ";
    $sql = $sql." CROSS JOIN (SELECT @row:=0) t7  ";
    $sql = $sql." ) SEQ  ";
    $sql = $sql." ) x   ";
    $sql = $sql." where (time(fecha) >= '".$horaIni."' and time((DATE_SUB(fecha, INTERVAL '".$frecuencia.":0' MINUTE_SECOND))) <= '".$horaFin."')   ";
    $sql = $sql." and date(fecha) = '".$fecha."') yy    ";
    $sql = $sql." where horasub >= '".$horaIni."' and horasub <= '".$horaFin."'";

                    //and horasub not in ('14:15', '15:00')";
      $query = $this->db->query($sql);

      /*foreach ($query->result_array() as $row)
      {
              echo $row['horasub'];
      }*/
      return $query->result_array();

  }

  function insertar($data)
  {
    try
    {
        $this->db->insert('servicio_profesional', $data);
        return TRUE;
    } catch (\Exception $e) {
      return FALSE;
    }
  }

  function existeAgenda($empresa, $sucursal, $servicio, $profesional)
  {
    $sSql = "";
    $sSql = "select count(1) existe ";
    $sSql = $sSql." from servicio_profesional ";
    $sSql = $sSql." where idEmpresa = ". $empresa;
    $sSql = $sSql." and idSucursal = ".$sucursal;
    $sSql = $sSql." and idServicio = ".$servicio;
    $sSql = $sSql." and idProfesional = ".$profesional;
    $sSql = $sSql." and vigencia = 'S' ";

    $query = $this->db->query($sSql);

    if ($query->num_rows() > 0)
    {
       $row = $query->row();
       $contador = intval($row->existe);

       if ($contador > 0)
       {
         return TRUE;
       }
    }
    return FALSE;

  }

}

<?php
  class Model_Servicio extends CI_Model
  {
    function insertar($data)
    {
      $this->db->insert('servicio', $data);
    }

    function GetAll()
    {
      $query = $this->db->get('servicio');
      return $query->result();
    }

    function verificar($data)
    {
      $query = $this->db->get('servicio');
      return $query->result();
    }

    function existeServicio($nombre, $descripcion, $empresa, $sucursal)
    {
      $sSql = "";
      $sSql = "select count(1) existe ";
      $sSql = $sSql." from servicio";
      $sSql = $sSql." where (upper(nombre) = upper('".$nombre."') or upper(descripcion) = upper('".$descripcion."'))";
      $sSql = $sSql." and idEmpresa =  ".$empresa;
      $sSql = $sSql." and idSucursal =  ".$sucursal;
      $sSql = $sSql." and vigencia = 'S'";

      $query = $this->db->query($sSql);

      if ($query->num_rows() > 0)
      {
         $row = $query->row();
         $contador = intval($row->existe);

         if ($contador > 0)
         {
           return TRUE;
         }
      }
      return FALSE;

    }
  }
?>

<?php
  class Model_Login extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
      $this->load->library('email');
    }

    function insertar($data)
    {
      $this->db->insert('usuario', $data);
    }

    function GetAll()
    {
      $query = $this->db->get('usuario');
      return $query->result();
    }

    public function login($usuario, $clave)
    {
        $this->db->where('correo',$usuario);
        $this->db->where('clave',sha1($clave));
        $this->db->where('vigencia','S');
        $query=$this->db->get('usuario');

        return $query->result();
    }

    public function empresaUsuario($us)
    {
        $empresa = 0;
        $usuario = 0;

        $this->db->where('idUsuario',$us);
        $this->db->where('fechaExpira >= NOW()');
        $query=$this->db->get('empresa');

        foreach ($query->result() as $row)
        {
          $empresa = $row->idEmpresa;
          $usuario = $row->idUsuario;
        }

        $this->db->where('idEmpresa',$usuario);
        $this->db->where('vigencia','S');
        $query=$this->db->get('sucursal');

        return $query->result();

    }

    Public function existeCorreo($correo)
    {
      $query_str="SELECT count(1) salida FROM usuario where correo = '".$correo."' limit 1";
      $query=$this->db->query($query_str);

      $record=$query->row();

      if($record->salida > 0)
      {
        return $record->salida;
      }
      else
      {
        return 0;
      }
    }

    public function enviaCorreoLogin($correo, $mensaje, $asunto)
    {
        $this->email->to($correo);
        $this->email->from('rincon.estilista@rinconestilista.cl');
        $this->email->subject($asunto);
        $this->email->message($mensaje);
        $this->email->send();

        return TRUE;
    }

    function updateClave($clave, $correo)
    {
        $laClave = sha1($clave);

        $this->db->set('clave', $laClave);
        $this->db->where('correo', $correo);
        if($this->db->update('usuario'))
        { return TRUE; }
        else
        { return FALSE; }

    }

  }

?>

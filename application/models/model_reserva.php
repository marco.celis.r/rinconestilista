<?php
// models/Users.php
  defined('BASEPATH') OR exit('No direct script access allowed');

  class Model_Reserva extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
      $this->load->library('email');
    }

    function insertar($data)
    {
      $this->db->insert('servicio', $data);
    }

    function GetAll()
    {
      $query = $this->db->get('servicio');
      return $query->result();
    }

    function verificar($data)
    {
      $query = $this->db->get('servicio');
      return $query->result();
    }

    public function get_current_page_records($limit, $start)
    {
        $this->db->limit($limit, $start);
        $query = $this->db->get("sucursal");

        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $data[] = $row;
            }

            return $data;
        }

        return false;
    }

    public function get_total()
    {
        return $this->db->count_all("sucursal");
    }

    /**
     * Retorna los datos de la empresa y sucursal
     *
     * @param int $idEmpresa
     * @param int $idSucursal
     * @return resultado
     */
    function getEmpresa($idEmpresa, $idSucursal)
    {
      $sSql = "";
      $sSql = " select nombreFantasia , descripcion, logo ";
      $sSql = $sSql." from sucursal  ";
      $sSql = $sSql." where idEmpresa =  ".$idEmpresa;
      $sSql = $sSql." and idSucursal = ".$idSucursal;
      $sSql = $sSql." and vigencia = 'S'";

      $query = $this->db->query($sSql);
      return $query->result_array();
    }


    function getAllServicio($idEmpresa, $idSucursal)
    {
      $sSql = "";
      $sSql = " select distinct sp.idServicio, se.nombre, se.descripcion ";
      $sSql = $sSql." from servicio_profesional sp ";
      $sSql = $sSql." , servicio se ";
      $sSql = $sSql." where sp.idEmpresa =  ".$idEmpresa;
      $sSql = $sSql." and sp.idSucursal	=  ".$idSucursal;
      $sSql = $sSql." and sp.vigencia = 'S' ";
      $sSql = $sSql." and se.idServicio = sp.idServicio ";
      $sSql = $sSql." and se.vigencia = 'S' ";

      $query = $this->db->query($sSql);
      return $query->result_array();
    }

    public function getProfesional($idEmpresa, $idSucursal, $idServicio)
    {
      $sSql = "";
      $sSql = " select * ";
      $sSql = $sSql." from servicio_profesional sp ";
      $sSql = $sSql." , profesional pr ";
      $sSql = $sSql." where sp.idEmpresa =  ".$idEmpresa;
      $sSql = $sSql." and sp.idSucursal	=  ".$idSucursal;
      $sSql = $sSql." and idServicio =  ".$idServicio;
      $sSql = $sSql." and sp.vigencia = 'S' ";
      $sSql = $sSql." and pr.idProfesional = sp.idProfesional  ";
      $sSql = $sSql." and pr.vigencia = 'S' ";

      $query = $this->db->query($sSql);
      return $query;
    }

    public function getAgenda($idEmpresa, $idSucursal, $idServicio, $idProfesional)
    {
      $sSql = "";
      $sSql = " select diasAtencion, horaInicio, horaFin, frecuencia, descanso, idServicioProfesional ";
      $sSql = $sSql." from servicio_profesional  ";
      $sSql = $sSql." where idEmpresa =  ".$idEmpresa;
      $sSql = $sSql." and idSucursal =  ".$idSucursal;
      $sSql = $sSql." and idServicio =  ".$idServicio;
      $sSql = $sSql." and idProfesional =  ".$idProfesional;
      $sSql = $sSql." and vigencia = 'S' ";

      //echo  "<br />getAgenda: ".$sSql."<br />";
      return $this->db->query($sSql);
    }


    public function generaHorario($intervalo, $horaIni, $horaFin, $hidServicioProfesional)
    {
        $descanso = "'12:00'";
        $sSql1 = "select descanso from servicio_profesional where idservicioProfesional = ".$hidServicioProfesional;
        foreach ($query = $this->db->query($sSql1)->result() as $row)
        {
          $descanso = $row->descanso;
        }

      $sSql = " select horasub from ( ";
      $sSql = $sSql." select fecha , time((DATE_SUB(fecha, INTERVAL '".$intervalo.":0' MINUTE_SECOND))) horasub ";
      $sSql = $sSql." from ( ";
      $sSql = $sSql." select STR_TO_DATE('2021-06-03 00:00:00','%Y-%m-%d %H:%i:%s') + INTERVAL ".$intervalo." * SEQ.row MINUTE AS Fecha ";
      $sSql = $sSql." FROM (SELECT  @row := @row + 1 as row ";
      $sSql = $sSql." FROM  (select 0 union all select 1 union all select 3 union all select 4 union all select 5 union all select 6 union all select 6 union all select 7 union all select 8 union all select 9) t ";
      $sSql = $sSql." CROSS JOIN (select 0 union all select 1 union all select 3 union all select 4 union all select 5 union all select 6 union all select 6 union all select 7 union all select 8 union all select 9) t2 ";
      $sSql = $sSql." CROSS JOIN (select 0 union all select 1 union all select 3 union all select 4 union all select 5 union all select 6 union all select 6 union all select 7 union all select 8 union all select 9) t3  ";
      $sSql = $sSql." CROSS JOIN (select 0 union all select 1 union all select 3 union all select 4 union all select 5 union all select 6 union all select 6 union all select 7 union all select 8 union all select 9) t4  ";
      $sSql = $sSql." CROSS JOIN (select 0 union all select 1 union all select 3 union all select 4 union all select 5 union all select 6 union all select 6 union all select 7 union all select 8 union all select 9) t5 ";
      $sSql = $sSql." CROSS JOIN (select 0 union all select 1 union all select 3 union all select 4 union all select 5 union all select 6 union all select 6 union all select 7 union all select 8 union all select 9) t6  ";
      $sSql = $sSql." CROSS JOIN (SELECT @row:=0) t7 ";
      $sSql = $sSql." ) SEQ ";
      $sSql = $sSql." ) x ";
      $sSql = $sSql." where (time(fecha) >= '".$horaIni."' and time((DATE_SUB(fecha, INTERVAL '".$intervalo.":0' MINUTE_SECOND))) <= '".$horaFin."') ";
      $sSql = $sSql." and date(fecha) = '2021-06-03' ";
      $sSql = $sSql." ) yy ";
      $sSql = $sSql." where horasub >= '".$horaIni."' and horasub <= '".$horaFin."' ";
      $sSql = $sSql." and horasub not in (".$descanso.") ";

      //echo "<br />Vinimos a generar el horario: ".$sSql;
      $query = $this->db->query($sSql);
      return $query;
    }

    public function getReservas($idEmpresa, $idSucursal, $idServicio, $idProfesional)
    {
      $sSql = "";
      $sSql = " select * ";
      $sSql = $sSql." from reserva ";
      $sSql = $sSql." where idEmpresa = 1 ";
      $sSql = $sSql." and idSucursal = 1 ";
      $sSql = $sSql." and idServicio = 2 ";
      $sSql = $sSql." and IdProfesional = 2 ";

      $query = $this->db->query($sSql);
      return $query;
    }

    public function traeFechas()
    {
      $sSql = "select DATE_FORMAT(now(), '%Y-%m-%d') hoy, DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 1 MONTH),  '%Y-%m-%d') manana";
      $query = $this->db->query($sSql);
      return $query;
    }

    /**
     * retornara el nombre del dia (in english)
     *
     * @param \date fecha
     * @return \String  Nombre del dia (en ingles)
     */
    public function nombreDia($fecha)
    {
      $sSql = "select upper(dayname('".$fecha."')) dia, DATE_FORMAT('".$fecha."','%d-%m-%Y') fechaEspa";
      $query = $this->db->query($sSql);
      return $query;
    }

    public function nombreEmpresa($idEmpresa, $idSucursal)
    {
      $sSql = "select nombreFantasia, logo from sucursal ";
      $sSql = $sSql." where idEmpresa =  ".$idEmpresa;
      $sSql = $sSql." and idSucursal =  ".$idSucursal;
      $sSql = $sSql." and vigencia = 'S'";
      $query = $this->db->query($sSql);
      return $query;
    }

    public function nombreProfesional($idProfesional)
    {
      $sSql = "select nombre from profesional ";
      $sSql = $sSql." where idProfesional =  ".$idProfesional;
      $sSql = $sSql." and vigencia = 'S' ";
      $query = $this->db->query($sSql);
      return $query;
    }

    function insertarReserva($data)
    {
      try
      {
          $this->db->insert('reserva', $data);
          return TRUE;
      } catch (Exception $e) {
        return FALSE;
      }
    }

    public function existeReserva($empresa, $sucursal, $servicio, $profesional, $fecha, $hora)
    {
      $sSql = "select count(1) existe ";
      $sSql = $sSql." from reserva ";
      $sSql = $sSql." where idEmpresa =  ".$empresa;
      $sSql = $sSql." and idSucursal =  ".$sucursal;
      $sSql = $sSql." and idServicio =  ".$servicio;
      $sSql = $sSql." and idProfesional =  ".$profesional;
      $sSql = $sSql." and fecha = '".$fecha."' ";
      $sSql = $sSql." and hora = '".$hora."' ";
      $sSql = $sSql." and estado = 'Vigente' ";

      //echo "<br />".$sSql."<br />";
      $existe = 0;
      foreach ($query = $this->db->query($sSql)->result() as $row)
      {
        $existe = $row->existe;
      }
      return $existe;
    }


    public function nombreServicio($servicio)
    {
      $sSql = "select descripcion from servicio where idServicio = ".$servicio." and vigencia = 'S'";
      $nombre = "";
      foreach ($query = $this->db->query($sSql)->result() as $row)
      {
        $nombre = $row->descripcion;
      }
      return $nombre;
    }


    public function nombreDelProfesional($profesional)
    {
      $sSql = "select nombre from profesional where idProfesional = ".$profesional." and vigencia = 'S'";
      $nombre = "";
      foreach ($query = $this->db->query($sSql)->result() as $row)
      {
        $nombre = $row->nombre;
      }
      return $nombre;
    }

    public function correoDelProfesional($profesional)
    {
      $sSql = "select correo from profesional where idProfesional = ".$profesional." and vigencia = 'S'";
      $correo = "";
      foreach ($query = $this->db->query($sSql)->result() as $row)
      {
        $correo = $row->correo;
      }
      return $correo;
    }

    public function correoDelUsuario($usuario)
    {
      $sSql = "select correo from usuario where idUsuario = ".$usuario." and vigencia = 'S'";
      $correo = "";
      foreach ($query = $this->db->query($sSql)->result() as $row)
      {
        $correo = $row->correo;
      }
      return $correo;
    }

    public function enviaCorreo($correo, $mensaje, $asunto)
    {
        $cuerpoCorreo = $mensaje;

        $this->email->to($correo);
        $this->email->from('rincon.estilista@rinconestilista.cl');
        $this->email->subject($asunto);
        $this->email->message($mensaje);
        //$this->email->send();

        if (! $this->email->send())
        {
          Echo "El proceso ha sido correcto pero No fue posible enviar el correo";
          return FALSE;
        }

        return TRUE;
    }


    public function reservasUsuario($usuario)
    {
      $sSql = "select re.idReserva, re.fecha, re.hora, pro.nombre, ser.descripcion, suc.nombreFantasia ";
      $sSql = $sSql." from reserva re ";
      $sSql = $sSql." , profesional pro ";
      $sSql = $sSql." , servicio ser ";
      $sSql = $sSql." , sucursal suc ";
      $sSql = $sSql." where re.idUsuario =  ".$usuario;
      $sSql = $sSql." and upper(re.estado) = 'VIGENTE' ";
      $sSql = $sSql." and pro.idProfesional = re.idProfesional ";
      $sSql = $sSql." and pro.vigencia = 'S' ";
      $sSql = $sSql." and ser.idServicio = re.idServicio ";
      $sSql = $sSql." and ser.vigencia = 'S' ";
      $sSql = $sSql." and suc.IdEmpresa = re.IdEmpresa ";
      $sSql = $sSql." and suc.vigencia = 'S' ";


      $query = $this->db->query($sSql);
      return $query;
    }

    public function datosReserva($reserva)
    {
      $sSql = "select re.idServicio, pro.idProfesional, re.idReserva, re.fecha, re.hora, pro.nombre, ser.descripcion ";
      $sSql = $sSql." from reserva re ";
      $sSql = $sSql." , profesional pro ";
      $sSql = $sSql." , servicio ser ";
      $sSql = $sSql." where re.idReserva =  ".$reserva;
      $sSql = $sSql." and upper(re.estado) = 'VIGENTE' ";
      $sSql = $sSql." and pro.idProfesional = re.idProfesional ";
      $sSql = $sSql." and pro.vigencia = 'S' ";
      $sSql = $sSql." and ser.idServicio = re.idServicio ";
      $sSql = $sSql." and ser.vigencia = 'S' ";

      $query = $this->db->query($sSql);
      return $query;
    }


    public function anulaReservaUsuario($idReserva, $idUsuario)
    {
      $sSql = " update reserva ";
      $sSql = $sSql." set estado = concat(idReserva,'|Anulado|usuario|".$idUsuario."|',fecha)";
      $sSql = $sSql." where idReserva = ".$idReserva;

      if(!$this->db->simple_query($sSql))
      {
        return FALSE;
      }
      return TRUE;
    }

    public function horaValida($hora)
    {
      $sSql = "select case when '".$hora."' < now() then 1 else 0 end salida";

      $valida = 0;
      foreach ($query = $this->db->query($sSql)->result() as $row)
      {
        $valida = $row->salida;
      }
      return $valida;
    }
  }
?>

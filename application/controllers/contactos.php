<?php

  class contactos extends CI_Controller
  {
    function __construct()
    {
      parent::__construct();
      $this -> load -> model('Model_Contactos');
      $this->load->library('email');
     }

     /*************************** Codificamos el string****************************************************/
     function encode($value)
     {
          if (!$value) {
              return false;
          }

          $key = sha1('EnCRypT10nK#Y!RiSRNn');
          $strLen = strlen($value);
          $keyLen = strlen($key);
          $j = 0;
          $crypttext = '';

          for ($i = 0; $i < $strLen; $i++) {
              $ordStr = ord(substr($value, $i, 1));
              if ($j == $keyLen) {
                  $j = 0;
              }
              $ordKey = ord(substr($key, $j, 1));
              $j++;
              $crypttext .= strrev(base_convert(dechex($ordStr + $ordKey), 16, 36));
          }

          return $crypttext;
      }

   /******************************* Decodifica ************************************************/
   function decode($value)
   {
          if (!$value) {
              return false;
          }

          $key = sha1('EnCRypT10nK#Y!RiSRNn');
          $strLen = strlen($value);
          $keyLen = strlen($key);
          $j = 0;
          $decrypttext = '';

          for ($i = 0; $i < $strLen; $i += 2) {
              $ordStr = hexdec(base_convert(strrev(substr($value, $i, 2)), 36, 16));
              if ($j == $keyLen) {
                  $j = 0;
              }
              $ordKey = ord(substr($key, $j, 1));
              $j++;
              $decrypttext .= chr($ordStr - $ordKey);
          }

          return $decrypttext;
      }

    /*************************** Enviaremos correo *****************************************/
    function sendMail($idUsuario)
    {
        $encrypted = $this->encode($idUsuario);

        $cuerpoCorreo = "<html><body> <center><img src='https://www.rinconestilista.cl/img/logo50.jpg' alt='Rincon Estilista' /></center><p>Hemos recibido tu solicitud para ser parte del la comunidad <b>Rinc&oacute;n Estilista</b></p><p>Para activar tu registro, debes dar clic al siguiente enlace:&nbsp;</p><p>
        <a href=".base_url()."contactos/verificar/".$encrypted.">Rinc&oacute;n Estilista</a>
        </p><p>Accediendo a nuestro sitio podr&aacute;s participar en las ofertas y eventos de nuestras empresas asociadas</p><p><b>Gracias por registrarte en El Rinc&oacute;n Estilista</b> </p><p><b></b> </p>	<p><b></b> </p></body></html>";

        $this->email->to($idUsuario);
        $this->email->from('rincon.estilista@rinconestilista.cl');
        $this->email->subject('Gracias por registrarte en Rincon Estilista');
        $this->email->message($cuerpoCorreo);
        $this->email->send();

    }

    public function index()
    {
        $borrarSesiones = 0;
        $data['activo'] = 'contactos';

        $idsession = $this->session->userdata('idusuario');
        if(!empty($idsession))
        {
           if($this->session->userdata('logged_in') == FALSE)
           {
             $data['logueado'] = FALSE;
             $data['nombreUsuario'] = $this->session->userdata('username');
             $data['eMail'] = $this->session->userdata('email');

             $this->sendMail($data['eMail']);
             $msg="Se envio un email a ".$data['eMail'];
             $borrarSesiones = 1;
           }
           else {
             $msg="Bienvenido ".$this->session->userdata('nombreUsuario');
           }
        }
        else
        {
            $errorRegistro = 0;
            $errorRegistro = intval($this->session->userdata('error_code'));

            if($errorRegistro > 0)
            {
                $msg = $this->session->userdata('merror_mensaje');
            }

        }

        $data['msg'] = $msg;
        $data['titulo'] = 'Pagina Principal';
        $this->load->view("plantilla/header", $data);
        $this->load->view("contactos/index");
        $this->load->view("plantilla/footer");

        if($borrarSesiones == 1)
        {
            //echo "El usuario no se encuentra verificado";
            try
            {
                $sess_array = $this->session->all_userdata();
                foreach($sess_array as $key =>$val)
                {
                   if($key!='session_id'
                      && $key!='last_activity'
                      && $key!='ip_address'
                      && $key!='user_agent'
                      && $key!='RESERVER_KEY_HERE')

                      //echo '<br/>nombre de sesion: '.$key.'     - Valor: '.$val;
                      $this->session->unset_userdata($key);
                }
            }
            catch (\Exception $e)
            {
                echo $e->getMessage();
            }
        }
    }

    public function verificar($id)
    {
        $data['activo'] = 'contactos';
        $decrypted = $this->decode($id);

        $vEstado = 'Verificado';
        $this->db->set('estado', $vEstado);
        $this->db->where('correo', $decrypted);
        $this->db->update('usuario');

        $data['titulo'] = 'Rincon Estilista';
        $data['idUsuario'] = $decrypted;
        $this->load->view("plantilla/header", $data);
        $this->load->view("contactos/verificar");
        $this->load->view("plantilla/footer");
    }

    public function editar()
    {
      $data['titulo'] = 'Pagina Editar';
      $data['activo'] = 'contactos';
      $this->load->view("plantilla/header", $data);
      $this->load->view("contactos/editar");
      $this->load->view("plantilla/footer");
    }

    public function agregar()
    {
      $data['titulo'] = 'Pagina Editar';
      $data['activo'] = 'registro';
      $this->load->view("plantilla/header", $data);
      $this->load->view("contactos/agregar");
      $this->load->view("plantilla/footer");
    }

    public function agregarContacto()
    {
      $camino='';
      $src='';
      $last_usuario =0;
      $data['activo'] = 'contactos';

      $this->form_validation->set_rules('nnombre','Nombre','required');
      $this->form_validation->set_rules('ndireccion','Direccion','required');
      $this->form_validation->set_rules('ntelefono','Telefono','required|numeric|trim|min_length[9]|max_length[10]');
      $this->form_validation->set_rules('nclave','Clave','required|trim|min_length[6]');
      $this->form_validation->set_rules('nclave2', 'Confirmacion de la clave', 'required|trim|matches[nclave]');
      $this->form_validation->set_rules('nfecha','Fecha de nacimiento','required');
      $this->form_validation->set_rules('ncorreo','Direccion de Correo','required|trim|valid_email');

      $tipou = intval($this->input->post('ntipo'));

      if($tipou == 1)
      {
        $this->form_validation->set_rules('ndescripcion','Descripcion','required');
        $this->form_validation->set_rules('nfantasia','Nombre de fantasia','required|trim|max_length[15]');
        $this->form_validation->set_rules('ndireccionE','Direccion de la Empresa','required');
        $this->form_validation->set_rules('ntelefonoE','Telefono de la Empresa','required');

        //$this->form_validation->set_rules('userfile', 'Logo de tu Empresa', 'trim|xss_clean|required');
        if (empty($_FILES['userfile']['name']))
        {
            $this->form_validation->set_rules('userfile', 'Logo de tu Empresa', 'required');
        }
      }



      if($this -> form_validation -> run() == FALSE)
      {
        $data['titulo'] = 'Pagina Principal';
        $this->load->view("plantilla/header", $data);
        $this->load->view("contactos/agregar");
        $this->load->view("plantilla/footer");
        return;
      }
      else
      {
        if($tipou == 1)
        {
          $config['upload_path']      = './uploads/';
          $config['allowed_types'] = '*';
          $config["max_size"]      = "100";
          $config["max_width"]     = "1024";
          $config["max_height"]    = "768";

          $this->load->library('upload', $config);
          $this->upload->set_allowed_types('jpg|jpeg|gif|png');
          $this->upload->initialize($config);

          $filename = $_FILES["userfile"]["name"];
          $file_ext = pathinfo($filename,PATHINFO_EXTENSION);
          $tipos = array("bmp", "jpg", "gif", "jpeg", "png");
          if (!in_array(strtolower($file_ext), $tipos))
          {
            $data['titulo'] = 'Registro de Usuarios';
            $data['msg'] = 'El archivo que intentaste subir no esta permitido';
            $this->load->view("plantilla/header", $data);
            $this->load->view("contactos/agregar");
            $this->load->view("plantilla/footer");
          }

          if ( ! $this->upload->do_upload('userfile'))
          {
                  $error = array('error' => $this->upload->display_errors());

                  $data['titulo'] = 'Registro de Usuarios';
                  $data['msg'] = 'Error al subir el archivo: '.$this->upload->display_errors();
                  $this->load->view("plantilla/header", $data);
                  $this->load->view("contactos/agregar");
                  $this->load->view("plantilla/footer");
                  return;
          }
          else
          {
                  $upload_data = array('upload_data' => $this->upload->data());
                  foreach ($this->upload->data() as $item=>$value):
                    //echo $item.' : '.$value.'<br/>';

                    if($item == 'full_path')
                    {
                      $camino = $value;
                    }
                  endforeach;

                  $path = $camino;
                  $type = pathinfo($path, PATHINFO_EXTENSION);
                  $data3 = file_get_contents($path);
                  $src = 'data:image/' . $type . ';base64,' . base64_encode($data3);

                  //echo "imagen: ".$src;
          }
        }

        $existeCorreo = $this -> Model_Contactos -> validaCorreo($this->input->post('ncorreo'));
        if (intval($existeCorreo) > 0)
        {
          $data['titulo'] = 'Registro de Usuarios';
          $data['msg'] = 'La direccion de correo ya se encuentra registrada';
          $this->load->view("plantilla/header", $data);
          $this->load->view("contactos/agregar");
          $this->load->view("plantilla/footer");
          return;
        }

        $data = array(
            'nombre'=>$this->input->post('nnombre'),
            'direccion'=>$this->input->post('ndireccion'),
            'telefono'=>$this->input->post('ntelefono'),
            'clave'=>sha1($this->input->post('nclave')),
            'fechaNacimiento'=>$this->input->post('nfecha'),
            'correo'=>$this->input->post('ncorreo'),
            'tipoUsuario'=>$tipou
        );

        try
        {
          if($this -> Model_Contactos -> insertar($data))
          {
              $last_usuario = $this->db->insert_id();
          }
        }
        catch (\Exception $e)
        {
          $newuser = array(
             'error_code'  => 404,
             'merror_mensaje'  => 'No fue posible regitrar al usuario',
             'logged_in' => FALSE
           );
        }

        if($last_usuario > 0)
        {
            $newuser = array(
               'username'  => $this->input->post('nnombre'),
               'email'     => $this->input->post('ncorreo'),
               'idusuario' => $last_usuario,
               'logged_in' => FALSE
             );

           $this->session->set_userdata($newuser);
        }

        if($tipou == 1 && $last_usuario > 0)
        {
          $uEmpresa = array(
           'idUsuario' => $last_usuario,
           );
          $this->db->set($uEmpresa);
          $last_empresa = $this->db->insert_id();

          /****** si no es posible crear a la empresa. eliminaremos el registro del usuario ***********/
          if (!$this->db->insert('empresa'))
          {
            $this -> Model_Contactos -> did_delete_row($last_usuario);
            $this->session->unset_userdata($newuser);

            $newuser = array(
               'error_code'  => 404,
               'merror_mensaje'  => 'No fue posible regitrar al usuario',
               'logged_in' => FALSE
             );

            $this->session->set_userdata($newuser);
          }
          else
          {
            $uSucursal = array(
              'idEmpresa' => $last_usuario,
              'nombreFantasia' => $this->input->post('nfantasia'),
              'descripcion' => $this->input->post('ndescripcion'),
              'direccion' => $this->input->post('ndireccionE'),
              'telefono' => $this->input->post('ntelefonoE'),
              'logo' => $src
             );

             try
             {
               $this->db->set($uSucursal);
               if (!$this->db->insert('sucursal'))
               {
                 $this -> Model_Contactos -> did_delete_row($last_usuario);

                 $this->db->where('idEmpresa', $last_empresa);
                 $this->db->delete('empresa');

                 $this->session->unset_userdata($newuser);

                 $newuser = array(
                    'error_code'  => 404,
                    'merror_mensaje'  => 'No fue posible regitrar a la empresa',
                    'logged_in' => FALSE
                  );
               }
             }
             catch (\Exception $e)
             {
               $this -> Model_Contactos -> did_delete_row($last_usuario);

               $this->db->where('idEmpresa', $last_empresa);
               $this->db->delete('empresa');

               $this->session->unset_userdata($newuser);

               $newuser = array(
                  'error_code'  => 404,
                  'merror_mensaje'  => 'No fue posible regitrar a la empresa',
                  'logged_in' => FALSE
                );
             }


          }
        }
        redirect(base_url().'contactos/');
      }
    }

    public function contactanos()
    {
      $data['activo'] = 'contactos';
      $data['titulo'] = 'Rincon Estilista - Contactanos';
      $data['msg'] = '';
      $this->load->view("plantilla/header", $data);
      $this->load->view("contactos/contactanos");
      $this->load->view("plantilla/footer");
    }

    public function enviarContacto()
    {
      $nombre = $this->input->post('nnombre');
      $mail = $this->input->post('nemail');
      $fono = $this->input->post('nnumero');
      $mensaje = $this->input->post('nmensaje');

      $cuerpoCorreo = "<html><body>
        <center><img src='https://www.rinconestilista.cl/img/logo50.jpg' alt='Rincon Estilista' /></center>
        <p>El usuario <b>".$nombre."</b></p>
        <p>Correo <b>".$mail."</b></p>
        <p>Fono <b>".$fono."</b></p>
        <p>Ha realizado la siguiente consulta: <br /><br /><b>".$mensaje."</b></p>
        </body></html>";

      $enviado = $this->mandaCorreosContacto($cuerpoCorreo);


      $data['activo'] = 'contactos';
      $data['titulo'] = 'Rincon Estilista - Contactanos';
      $data['msg'] = 'Tu consulta ha sido enviada<br />Gracias por contactarte con nosotros!';
      $this->load->view("plantilla/header", $data);
      $this->load->view("contactos/contactanos");
      $this->load->view("plantilla/footer");
    }

    public function mandaCorreosContacto($cuerpoCorreo)
    {
      require_once('class.smtp.php');
      require_once('class.phpmailer.php');

      $from  = "rincon.estilista@rinconestilista.cl";
      $namefrom = "Rincon Estilista";
      $subject = "Consulta desde tu sitio";
      $mail = new PHPMailer();
      $mail->CharSet = 'iso-8859-1'; //'UTF-8';
      $mail->isSMTP();   // by SMTP
      $mail->SMTPAuth   = true;   // user and password
      $mail->Host       = "mail.rinconestilista.cl";
      $mail->Port       = 25;
      $mail->Username   = $from;
      $mail->Password   = "Macr070979!";
      $mail->SMTPSecure = "";    // options: 'ssl', 'tls' , ''
      $mail->setFrom($from,$namefrom);   // From (origin)
      $mail->addCC($from,$namefrom);      // There is also addBCC
      $mail->Subject  = $subject;
      //$mail->AltBody  = $altmess;
      $mail->Body = $cuerpoCorreo;
      $mail->isHTML();   // Set HTML type
    //$mail->addAttachment("attachment");
      $mail->addAddress("zaleitarot@gmail.com", "Rocio");
      return $mail->send();


      return true;

    }
  }
?>

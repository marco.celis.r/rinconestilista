<?php
class promocion extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this -> load -> model('Model_Promocion');
  }

  public function index()
  {
    $data['activo'] = 'empresa';
    $data['titulo'] = 'Rincon Estilista - Promociones';
    $data['msg'] = '';
    $this->load->view("plantilla/header", $data);
    $this->load->view("promocion/index");
    $this->load->view("plantilla/footer");
  }

  public function subirImagen()
  {
    $this->form_validation->set_rules('ndescripcion','Descripcion de la promocion','required');
    $this->form_validation->set_rules('nvalorante','Valor anterior','required');
    $this->form_validation->set_rules('nvaloroferta','Valor oferta','required');

    if($this -> form_validation -> run() == FALSE)
    {
      $data['activo'] = 'empresa';
      $data['titulo'] = 'Rincon Estilista - Promociones';
      $data['msg'] = 'Todos los datos son obligatorios';
      $this->load->view("plantilla/header", $data);
      $this->load->view("promocion/index");
      $this->load->view("plantilla/footer");
      return;
    }

    $antes = $this->input->post('nvalorante');
    $promo = $this->input->post('nvaloroferta');

    if(intval($antes) <= intval($promo))
    {
      $data['activo'] = 'empresa';
      $data['titulo'] = 'Rincon Estilista - Promociones';
      $data['msg'] = 'El valor de la oferta debe ser inferior al valor actual';
      $this->load->view("plantilla/header", $data);
      $this->load->view("promocion/index");
      $this->load->view("plantilla/footer");
      return;
    }

    $sess_empresa = $this->session->userdata('idusuario');
    $sess_sucursal = $this->session->userdata('idusuario');

    $type = pathinfo($_FILES['imagen']['tmp_name'], PATHINFO_EXTENSION);
    $datos = file_get_contents($_FILES['imagen']['tmp_name']);
    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($datos);

    $newPromo = array(
        'imagen'=>$base64,
        'descripcion'=>$this->input->post('ndescripcion'),
        'valorAnterior'=>$this->input->post('nvalorante'),
        'valorPromocion'=>$this->input->post('nvaloroferta'),
        'idEmpresa'=>$sess_empresa,
        'idSucursal'=>$sess_sucursal
    );

    if($this -> Model_Promocion -> insertar($newPromo))
    {
        $data['msg'] = 'Se ha generado una nueva promocion! ';
    }
    else
    {
        $data['msg'] = 'No fue posible crear la promocion ';
    }

    $data['activo'] = 'empresa';
    $data['titulo'] = 'Rincon Estilista - Promociones';
    $this->load->view("plantilla/header", $data);
    $this->load->view("promocion/index");
    $this->load->view("plantilla/footer");

    //echo "<img style='display:block; width:100px;height:100px;' id='base64image' src='".$base64."' alt='Promocion'/>";
  }
}
?>

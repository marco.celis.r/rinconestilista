<?php
class logout extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
   }

  public function index()
  {
    $data['activo'] = 'logout';
    try
    {
        $sess_array = $this->session->all_userdata();
        foreach($sess_array as $key =>$val)
        {
           if($key!='session_id'
              && $key!='last_activity'
              && $key!='ip_address'
              && $key!='user_agent'
              && $key!='RESERVER_KEY_HERE')

              //echo '<br/>nombre de sesion: '.$key.'     - Valor: '.$val;
              $this->session->unset_userdata($key);
        }
    }
    catch (\Exception $e)
    {
        echo $e->getMessage();
    }

    //echo "Vinimos";
    $data['titulo'] = 'Rincon estilista - Login.';
    $this->load->view("plantilla/header", $data);
    $this->load->view("logout/index");
    $this->load->view("plantilla/footer");

  }
}
 ?>

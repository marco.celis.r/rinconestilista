<?php
class servicio extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this -> load -> model('Model_Servicio');
  }

  public function index()
  {
    $data['activo'] = 'servicio';
    $data['titulo'] = 'Rincon Estilista - Servicios';
    $data['msg'] = '';
    $this->load->view("plantilla/header", $data);
    $this->load->view("servicio/index");
    $this->load->view("plantilla/footer");
  }

  public function agregarservicio()
  {
    $data['activo'] = 'servicio';
    $this->form_validation->set_rules('nnombre','Nombre','required|max_length[100]');
    $this->form_validation->set_rules('ndescripcion','Descripcion','required|max_length[100]');

    if($this -> form_validation -> run() == FALSE)
    {
      $data['titulo'] = 'Rincon Estilista - Servicios';
      $data['msg'] = '';
      $this->load->view("plantilla/header", $data);
      $this->load->view("servicio/agregar");
      $this->load->view("plantilla/footer");
    }
    else
    {
      $data = array(
          'nombre'=>$this->input->post('nnombre'),
          'descripcion'=>$this->input->post('ndescripcion')
      );

      $this -> Model_Servicio -> insertar($data);
      $last_usuario = $this->db->insert_id();

      if($last_usuario > 0)
      {
        $data['titulo'] = 'Rincon Estilista - Servicio';
        $data['msg'] = 'El servicio ha sido registrado exitosamente ('.$last_usuario.')';
        $this->load->view("plantilla/header", $data);
        $this->load->view("servicio/index");
        $this->load->view("plantilla/footer");
      }

    }
  }

}
 ?>

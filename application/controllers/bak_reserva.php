<?php
class reserva extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->library('pagination');
    $this->load->helper('url');
    $this -> load -> model('Model_Reserva');
  }

  public function index()
  {
    $data['activo'] = 'reserva';
    $data['titulo'] = 'Rincon Estilista - Reservas';
    $data['msg'] = '';
    $this->load->view("plantilla/header", $data);
    $this->load->view("reserva/index");
    $this->load->view("plantilla/footer");
  }


}
 ?>

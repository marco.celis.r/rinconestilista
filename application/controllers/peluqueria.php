<?php
class peluqueria extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
   }

  public function index()
  {
    $data['activo'] = 'empresa';
    $data['msg'] = '';
    $data['titulo'] = 'Rincon estilista - Peluqueria.';
    
    $this->load->view("plantilla/header", $data);
    $this->load->view("peluqueria/index");
    $this->load->view("plantilla/footer");

  }
}
 ?>

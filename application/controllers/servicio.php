<?php
class servicio extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this -> load -> model('Model_Servicio');
  }

  public function index()
  {
    $data['activo'] = 'empresa';
    $data['titulo'] = 'Rincon Estilista - Servicios';
    $data['msg'] = '';
    $this->load->view("plantilla/header", $data);
    $this->load->view("servicio/index");
    $this->load->view("plantilla/footer");


    	/*try
      {
      $sess_array = $this->session->all_userdata();
      foreach($sess_array as $key =>$val)
      {
      if($key!='session_id'
      && $key!='last_activity'
      && $key!='ip_address'
      && $key!='user_agent'
      && $key!='RESERVER_KEY_HERE')

      echo '<br/>nombre de sesion: '.$key.' - Valor: '.$val;
      //$this->session->unset_userdata($key);
      }
      }
      catch (\Exception $e)
      {
      echo $e->getMessage();
    }*/
  }

  public function agregarservicio()
  {
    $idEmpresa= $this->session->userdata('idEmpresa');
    $idSucursal= $this->session->userdata('idSucursal');

    /*if($idEmpresa == 0 || $idSucursal == 0)
    {
      echo "No tiene empresa (".$idEmpresa.") o sucursal(".$idSucursal.")";
      return;
    }*/

    $data['activo'] = 'empresa';
    $this->form_validation->set_rules('nnombre','Nombre','required|max_length[100]');
    $this->form_validation->set_rules('ndescripcion','Descripcion','required|max_length[100]');

    if($this -> form_validation -> run() == FALSE)
    {
      $data['activo'] = 'empresa';
      $data['titulo'] = 'Rincon Estilista - Servicios';
      $data['msg'] = '';
      $this->load->view("plantilla/header", $data);
      $this->load->view("servicio/index");
      $this->load->view("plantilla/footer");
    }
    else
    {

      if($this -> Model_Servicio -> existeServicio($this->input->post('nnombre'), $this->input->post('ndescripcion'), $idEmpresa, $idSucursal))
      {
        $data['activo'] = 'empresa';
        $data['titulo'] = 'Rincon Estilista - Servicios';
        $data['msg'] = 'El Servicio ya se encuentra registrado';
        $this->load->view("plantilla/header", $data);
        $this->load->view("servicio/index");
        $this->load->view("plantilla/footer");
        return;
      }

      $data = array(
          'nombre'=>$this->input->post('nnombre'),
          'descripcion'=>$this->input->post('ndescripcion'),
          'idEmpresa'=>$idEmpresa,
          'idSucursal'=>$idSucursal
      );

      $this -> Model_Servicio -> insertar($data);
      $last_usuario = $this->db->insert_id();

      if($last_usuario > 0)
      {
        $data['activo'] = 'empresa';
        $data['titulo'] = 'Rincon Estilista - Servicio';
        $data['msg'] = 'El servicio ha sido registrado exitosamente ('.$last_usuario.')';
        $this->load->view("plantilla/header", $data);
        $this->load->view("servicio/index");
        $this->load->view("plantilla/footer");
      }

    }
  }

}
 ?>

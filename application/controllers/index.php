<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<style>
  #intro {
    background-image: url(https://micarrerauniversitaria.com/wp-content/uploads/2018/04/Hair-Salon-Insurance-5.jpg);
    height: 120vh;
  }

  /* Height for devices larger than 576px */
  @media (min-width: 992px) {
    #intro {
      margin-top: -58.59px;
    }
  }
</style>


<br>
<br>
<div id="intro">
  <div class="Container">
    <br>
    <div class="row justify-content-center">
      <div class="col-xl-5 col-md-8">
        <div class="bg-white  rounded-5 shadow-5-strong p-5">
          <center>
            <h3>Agregar Profesionales</h3>
          </center>
          <br>
          <?php echo form_open_multipart(base_url() . 'profesional/agregarprofesional'); ?>
          <div class="col-md-12 col-md_offset-4">

            <div class="form-group">
              <label for="nnombre">Nombre</label>
              <input type="text" class="form-control" id="nnombre" name="nnombre" aria-describedby="nnombreHelp" placeholder="Nombre del profesional">
              <small id="nnombreHelp" class="form-text text-muted" Este dato es obligatorio.</small>
            </div>
<br>
            <div class="form-group">
              <label for="ntelefono">Telefono</label>
              <input type="text" class="form-control" id="ntelefono" name="ntelefono" min="99999999" max="999999999" placeholder="numero de 9 digitos" required onkeyup="textCounter(this,'counter',9);" onblur="ocultar();" onfocus="mostrar()">
            </div>
<br>
            <div class="form-group">
              <label for="ncorreo">Direccion de Correo Electronico</label>
              <input type="mail" class="form-control" id="ncorreo" name="ncorreo" placeholder="Direccion de Correo">
            </div>
            <br>

        
			
			
            <div class="Container">
              <div class="row">
                <div class="col-md-12">
                  <center>
                    <?php
                    echo "<br /><center>" . $msg . "</center>";
                    ?>
                  </center>
                </div>
              </div>
            </div>
<br /><br />
            <button type="submit" class="btn btn-primary">Guardar</button>

          </div>


          <br />

          <div class="row">
            <div class="col-4">
              <?php echo validation_errors(); ?>
            </div>
          </div>

        </div>
        <br />
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  (function() {
    'use strict'

    var forms = document.querySelectorAll('.needs-validation')

    Array.prototype.slice.call(forms)
      .forEach(function(form) {
        form.addEventListener('submit', function(event) {
          if (!form.checkValidity()) {
            event.preventDefault()
            event.stopPropagation()
          }

          form.classList.add('was-validated')
        }, false)
      })
  })()



  $("input[type=date]").datepicker({
    dateFormat: 'yy-mm-dd',
    onSelect: function(dateText, inst) {
      $(inst).val(dateText);
    }
  });

  $("input[type=date]").on('click', function() {
    return false;
  });

  function textCounter(field, field2, maxlimit) {
    var countfield = document.getElementById(field2);
    if (field.value.length > maxlimit) {
      field.value = field.value.substring(0, maxlimit);
      return false;
    } else {
      countfield.value = maxlimit - field.value.length + " Digitos";
    }
  }

  function ocultar() {
    document.getElementById("counter").style.display = 'none';
  }

  function mostrar() {
    document.getElementById("counter").style.display = 'block';
  }
</script>
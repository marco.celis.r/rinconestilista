<?php if ( ! defined('BASEPATH')) exit('No se permite el redireccionamiento');

class Home extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this -> load -> model('Model_Promocion');
   }

 public function index()
	{
    $data['promos'] = $this->Model_Promocion->listaPromociones();
    $data['titulo'] = 'Home';
		$data['activo'] = 'home';
    $this->load->view("plantilla/header", $data);
    $this->load->view('home/home');
    $this->load->view("plantilla/footer");

	}
}
?>

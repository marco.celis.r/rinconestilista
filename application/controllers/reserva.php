<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reserva extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('pagination');
        $this->load->helper('url');
        $this -> load -> model('Model_Reserva');
      }

      /**
       * Permite retornar la fecha como string en español
       *
       * @param \DateTime $fecha
       * @return \String  Fecha en literal español
       */
      function fechaCastellano ($fecha)
      {
        $fecha = substr($fecha, 0, 10);
        $numeroDia = date('d', strtotime($fecha));
        $dia = date('l', strtotime($fecha));
        $mes = date('F', strtotime($fecha));
        $anio = date('Y', strtotime($fecha));
        $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
        $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
        $nombredia = str_replace($dias_EN, $dias_ES, $dia);
        $meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $meses_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        $nombreMes = str_replace($meses_EN, $meses_ES, $mes);
        return $nombredia." ".$numeroDia." de ".$nombreMes." de ".$anio;
      }

    public function index()
    {
      if($sess_id = $this->session->userdata('estado') == 'No Verificado')
      {
        "Usuario no verificado, por favor verifique su direccion de correo";
        $data['noVerificado'] = "Usuario no verificado, por favor verifique su direccion de correo";
      }
        $data['activo'] = 'reserva';
        // llama la db y el modelo
        $this->load->database();
        $this->load->model('Model_Reserva');

        // init params
        $params = array();
        $limit_per_page = 2;
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $total_records = $this->Model_Reserva->get_total();

        if ($total_records > 0)
        {
            // get current page records
            $params["results"] = $this->Model_Reserva->get_current_page_records($limit_per_page, $start_index);

            $config['base_url'] = base_url() . 'reserva/index';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;

            $this->pagination->initialize($config);

            // build paging links
            $params["links"] = $this->pagination->create_links();
        }

        $data['titulo'] = 'Rincon estilista - Reservas';
        $data['msg'] = '';

        $this->load->view("plantilla/header", $data);
        $this->load->view("reserva/index", $params);
        $this->load->view("plantilla/footer");
    }

    public function custom()
    {
        $data['activo'] = 'login';
        // load db and model
        $this->load->database();
        $this->load->model('sucursal');

        // init params
        $params = array();
        $limit_per_page = 2;
        $page = ($this->uri->segment(3)) ? ($this->uri->segment(3) - 1) : 0;
        $total_records = $this->Model_Reserva->get_total();

        if ($total_records > 0)
        {
            // get current page records
            $params["results"] = $this->Model_Reserva->get_current_page_records($limit_per_page, $page*$limit_per_page);

            $config['base_url'] = base_url() . 'reserva/custom';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;

            // custom paging configuration
            $config['num_links'] = 2;
            $config['use_page_numbers'] = TRUE;
            $config['reuse_query_string'] = TRUE;

            $config['full_tag_open'] = '<div class="pagination">';
            $config['full_tag_close'] = '</div>';

            $config['first_link'] = 'First Page';
            $config['first_tag_open'] = '<span class="firstlink">';
            $config['first_tag_close'] = '</span>';

            $config['last_link'] = 'Last Page';
            $config['last_tag_open'] = '<span class="lastlink">';
            $config['last_tag_close'] = '</span>';

            $config['next_link'] = 'Next Page';
            $config['next_tag_open'] = '<span class="nextlink">';
            $config['next_tag_close'] = '</span>';

            $config['prev_link'] = 'Prev Page';
            $config['prev_tag_open'] = '<span class="prevlink">';
            $config['prev_tag_close'] = '</span>';

            $config['cur_tag_open'] = '<span class="curlink">';
            $config['cur_tag_close'] = '</span>';

            $config['num_tag_open'] = '<span class="numlink">';
            $config['num_tag_close'] = '</span>';

            $this->pagination->initialize($config);

            // build paging links
            $params["links"] = $this->pagination->create_links();
        }

        //$this->load->view('user_listing', $params);

        $data['titulo'] = 'Rincon estilista - Reservas';
        $data['msg'] = '';

        $this->load->view("plantilla/header", $data);
        $this->load->view("reserva/index", $params);
        $this->load->view("plantilla/footer");
    }

    public function empresaSucursal()
    {

      /*************************************************************************
      * validaremos que se encuentre una sesion iniciada
      *************************************************************************/
      $sess_id = $this->session->userdata('email');
      $sess_sucursal = $this->session->userdata('idusuario');
      $data['activo'] = 'reserva';

      if(empty($sess_id))
      {
        $data['titulo'] = 'Login';
        $data['msg'] = '';

        $this->load->view('plantilla/header', $data);
        $this->load->view('login/index');
        $this->load->view('plantilla/footer');
        return;
      }
      /************************************************************************/
      $data['idEmpresa'] = $this->input->get('empresa');
      $data['idSucursal'] = $this->input->get('sucursal');

      $idEmpresa = $this->input->get('empresa');
      $idSucursal = $this->input->get('sucursal');

      $i = 0;
      foreach ($this->Model_Reserva->getEmpresa($idEmpresa, $idSucursal) as $row)
      {
          $i = $i + 1;
          $data['nombreFantasia'] = $row['nombreFantasia'];
          $data['descripcion']  = $row['descripcion'];
          $data['logo']  = $row['logo'];
      }

      if($i == 0)
      {
        $data['titulo'] = 'Rincon estilista - Reservas';
        $data['msg'] = 'No se encontro informacion para esta empresa';
        $data['buscar'] = 'buscar';
        $data['activo'] = 'reserva';
        $this->load->view("plantilla/header", $data);
        $this->load->view("reserva/index");
        $this->load->view("plantilla/footer");
        return;
      }

      $data['titulo'] = 'Rincon estilista - Reservas';
      $data['msg'] = "";
      $data['buscar'] = 'buscar';
      $data['activo'] = 'reserva';
      $data['servicios'] = $this->Model_Reserva->getAllServicio($idEmpresa,$idSucursal);

      $this->load->view("plantilla/header", $data);
      $this->load->view("reserva/index");
      $this->load->view("plantilla/footer");
    }

    public function buscaServicios()
    {
      //echo "Vininos a buscar servicios";
      $empresa = $_GET['empresaId'];
      $sucursal = $_GET['sucursalId'];
      $servicio = $_GET['servicioId'];

      $query = $this->Model_Reserva->getProfesional($empresa,$sucursal,$servicio);

      if ($query->result() > 0)
      {
        print "<option value=''>-- SELECCIONE --</option>";
        foreach ($query->result() as $row)
        {
          	print "<option value='$row->idProfesional'>$row->nombre</option>";
        }
      }
      else
      {
        print "<option value=''>-- NO HAY DATOS --</option>";
      }
    }

    public function buscaAgenda()
    {
      $horaIni = "00:00";
      $horaFin = "00:00";
      $intervalo = 30;
      $descanso = "'12:00'";

      /*echo "<br />Vinimos a buscar la agenda: ".$_GET['profesionalId'];
      echo "<br />Vinimos con el id de servicio: ".$_GET['servicioId'];
      echo "<br />Vinimos con el id Empresa: ".$_GET['empresaId'];
      echo "<br />Vinimos con el id Sucursal: ".$_GET['sucursalId'];
      echo "<br />Buscaremos la agenda del profesional: ".$_GET['profesionalId'];*/

      $horario = $this->Model_Reserva->getAgenda($_GET['empresaId'],$_GET['sucursalId'], $_GET['servicioId'], $_GET['profesionalId']);

      if ($horario->result() > 0)
      {
        foreach ($horario->result() as $row)
        {
            $dias = $row->diasAtencion;
            $horaIni = $row->horaInicio;
            $horaFin = $row->horaFin;
            $intervalo = $row->frecuencia;
            $idServicioProfesional = $row->idServicioProfesional;
            $descanso = $row->descanso;
        }

        echo "<input type='hidden' id='hidServicio' name='hidServicio' value='".$_GET['servicioId']."' />";
        echo "<input type='hidden' id='hidProfesional' name='hidProfesional' value='".$_GET['profesionalId']."' />";
        echo "<input type='hidden' id='hidDias' name='hidDias' value='".$dias."' />";
        echo "<input type='hidden' id='hidHoraIni' name='hidHoraIni' value='".$horaIni."' />";
        echo "<input type='hidden' id='hidHoraFin' name='hidHoraFin' value='".$horaFin."' />";
        echo "<input type='hidden' id='hidFrecuencia' name='hidFrecuencia' value='".$intervalo."' />";
        echo "<input type='hidden' id='hidDescanso' name='hidDescanso' value='".$descanso."' />";
        echo "<input type='hidden' id='hidEmpresa1' name='hidEmpresa1' value='".$_GET['empresaId']."' />";
        echo "<input type='hidden' id='hidSucursal1' name='hidSucursal1' value='".$_GET['sucursalId']."' />";
        echo "<input type='hidden' id='hidServicioProfesional' name='hidServicioProfesional' value='".$idServicioProfesional."' />";
        echo "<br /><br />";

        $hoy = date("Y-m-d");
        $despues = date("Y-m-d");
        $lasFechas = $this->Model_Reserva->traeFechas();
        foreach ($lasFechas->result() as $row)
        {
          $hoy = $row->hoy;
          $despues = $row->manana;
        }
        echo "<label for='start'>Start date:</label>";
        echo "<input type='date' id='calendario' name='calendario' value='".$hoy."' min='".$hoy."' max='".$despues."'>";

        echo "<input type='submit' value='Buscar Hora'>";
      }
    }

    public function reservaServicio()
    {
      $data['activo'] = 'reserva';
      $data['titulo'] = 'Rincon estilista - Reservas';
      $data['buscar'] = 'Seleccionar Horario';
      $data['msg'] =  "";

      $hidEmpresa =     $data['hidEmpresa1']     = $_POST['hidEmpresa1'];
      $hidSucursal=     $data['hidSucursal1']    = $_POST['hidSucursal1'];
      $hidServicio =    $data['hidServicio']    = $_POST['hidServicio'];
      $hidProfesional = $data['hidProfesional'] = $_POST['hidProfesional'];
      $hidDias =        $data['hidDias']        = $_POST['hidDias'];
      $hidHoraIni =     $data['hidHoraIni']     = $_POST['hidHoraIni'];
      $hidHoraFin =     $data['hidHoraFin']     = $_POST['hidHoraFin'];
      $hidFrecuencia =  $data['hidFrecuencia']  = $_POST['hidFrecuencia'];
      $hidDescanso =    $data['hidDescanso']    = $_POST['hidDescanso'];
      $hidFecha =       $data['hidFecha']       = $_POST['calendario'];
      $hidServicioProfesional =       $data['hidServicioProfesional']       = $_POST['hidServicioProfesional'];

      /**
       * Validaremos que el profesional
       *Atienda el dia seleccionado
       *
       * @param \Date $hidFecha
       * @return \String Nombre del dia seleccionado
       */
      $lasFechas = $this->Model_Reserva->nombreDia($hidFecha);
      $nombreDia = "";
      $fechaEspa = "";
      $laFecha = "";
      foreach ($lasFechas->result() as $row)
      {
        $nombreDia = $row->dia;
        $fechaEspa = $row->fechaEspa;
      }

      $pos = strpos($hidDias, $nombreDia);
      if ($pos === false) {
          //Si el profesional no atiende, retornamos el mensaje correspondiente
          //y retornamos la vista correspondiente
          $data['msg'] = "El Profesional NO atiende este dia";
          $this->load->view("plantilla/header", $data);
          $this->load->view("reserva/reservaServicio");
          $this->load->view("plantilla/footer");
          return;
      } else {
        //Obtenemos un string con la fechaEspa
        $laFecha = $this -> fechaCastellano($fechaEspa);

        /*
        *Traemos los datos de la empresa
        */
        $nombreEmpresa = "";
        $logoEmpresa ="";
        $laEmpresa = $this->Model_Reserva->nombreEmpresa($hidEmpresa, $hidSucursal);
        foreach ($laEmpresa->result() as $row)
        {
          $nombreEmpresa = $row->nombreFantasia;
          $logoEmpresa = $row->logo;
        }

        /*
        * Traemos los datos del profesional
        */
        $nombreProfesional = "";
        $elProfesional = $this->Model_Reserva->nombreProfesional($hidProfesional);
        foreach ($elProfesional->result() as $row)
        {
          $elProfesional = $row->nombre;
        }

        $data['estaReserva'] = "Su reserva es con el profesional ".$elProfesional
                        ."<br /> De la empresa ".$nombreEmpresa
                        ."<br /> Para el dia ".$laFecha;

        $data['horas'] = $this->Model_Reserva->generaHorario($hidFrecuencia, $hidHoraIni, $hidHoraFin, $hidServicioProfesional);
      }

      $this->load->view("plantilla/header", $data);
      $this->load->view("reserva/reservaServicio");
      $this->load->view("plantilla/footer");
    }

    /**
     * Permite generar la reserva
     *
     * @param \int idUsuario
     * @param \int idSucursal
     * @param \int idServicio
     * @param \int idProfesional
     * @param \Date fecha
     * @param \Time hora
     * @param \String estado
     * @param \int IdEmpresa
     * @return \var  Retornara la reserva generada
     */
    public function realizarReserva()
    {
      $data['titulo'] = 'Rincon estilista - Reservas';
      $horaReserva = $this->input->post("horaReserva");
      $idUsuario      = $this->session->userdata('idusuario');
      $nombreUsuario  = $this->session->userdata('nombreUsuario');
      $idSucursal = $_POST['idSucursal'];
      $idServicio = $_POST['idServicio'];
      $idProfesional = $_POST['idProfesional'];
      $fecha = $_POST['fecha'];
      $hora = $horaReserva;
      $estado = 'Vigente';
      $IdEmpresa = $_POST['IdEmpresa'];

      $data = array(
          'idUsuario' => $idUsuario,
          'idSucursal' => $idSucursal,
          'idServicio' => $idServicio,
          'idProfesional' => $idProfesional,
          'fecha' => $fecha,
          'hora' => $hora,
          'estado' => $estado,
          'IdEmpresa' => $IdEmpresa
      );

      try
      {
        $Existe = $this->Model_Reserva->existeReserva($IdEmpresa
              , $idSucursal
              , $idServicio
              , $idProfesional
              , $fecha
              , $hora
            );

        if(intval($Existe)>0)
        {
          $data['msg'] = "Esta hora ya se encuentra reservada<br />Por favor, selecciona otra hora";
          $data['activo'] = 'reserva';
          $this->load->view("plantilla/header", $data);
          $this->load->view("reserva/reservarealizada");
          $this->load->view("plantilla/footer");
          return;
        }

        if($this -> Model_Reserva -> insertarReserva($data))
        {
            $last_reserva = $this->db->insert_id();
        }
      }
      catch (\Exception $e)
      {
        $newuser = array(
           'error_code'  => 404,
           'merror_mensaje'  => 'No fue posible reservar su hora',
           'logged_in' => FALSE
         );
      }

      if(isset($last_reserva))
      {
        $laEmpresa = $this->Model_Reserva->nombreEmpresa($IdEmpresa, $idSucursal);foreach ($laEmpresa->result() as $row)
        {
          $nombreEmpresa = $row->nombreFantasia;
          $logoEmpresa = $row->logo;
        }

        $lasFechas = $this->Model_Reserva->nombreDia($fecha);
        $nombreDia = "";
        $fechaEspa = "";
        $laFecha = "";
        foreach ($lasFechas->result() as $row)
        {
          $nombreDia = $row->dia;
          $fechaEspa = $row->fechaEspa;
        }
        $laFecha = $this -> fechaCastellano($fechaEspa);

        $nombreServicio = $this->Model_Reserva->nombreServicio($idServicio);
        $nombreProfesional = $this->Model_Reserva->nombreDelProfesional($idProfesional);
        $correoProfesional = $this->Model_Reserva->correoDelProfesional($idProfesional);
        $correoUsuario = $this->Model_Reserva->correoDelUsuario($idUsuario);

        $salida = "<br />Hemos generado la reserva numero ".$last_reserva;
        $salida = $salida."<br />Para el dia ".$laFecha.", a las ".$horaReserva;
        $salida = $salida."<br />A nombre del Usuario ".$nombreUsuario;
        $salida = $salida."<br />Su atencion corresponde a un servicio de ".$nombreServicio;
        $salida = $salida."<br />El nombre del profesional que lo atendera es ".$nombreProfesional;
        $salida = $salida."<br /><br />Por favor recuerde ser puntual y presentese en la recepcion";
        $salida = $salida."<br />al menos con 15 minutos de anticipacion para registrar su llegada";
        $salida = $salida."<br /><br />Si no puede asistir a su atencion, por favor cancele su reserva";
        $salida = $salida."<br />y libere la hora para alguien que podria necesitarla";
        $salida = $salida."<br /><br />Atentamente";
        $salida = $salida."<br />Su salon de belleza ".$nombreEmpresa;
        $data['salida'] = $salida;

        $cuerpoCorreo = "<html><body> <center><img src='https://www.rinconestilista.cl/img/logo50.jpg' alt='Rincon Estilista' /></center>";
        $cuerpoCorreo = $cuerpoCorreo."<br />".$salida;
        $cuerpoCorreo = $cuerpoCorreo."<br /></body></html>";

        $enviado = $this->Model_Reserva->enviaCorreo($correoUsuario, $cuerpoCorreo, "Gracias por reservar con nosotros");

        $salida2 = "<br />Se ha generado la reserva numero ".$last_reserva;
        $salida2 = $salida2."<br />Para el dia ".$laFecha.", a las ".$horaReserva;
        $salida2 = $salida2."<br />A nombre del Usuario ".$nombreUsuario;
        $salida2 = $salida2."<br />La atencion corresponde a un servicio de ".$nombreServicio;
        $salida2 = $salida2."<br /><br />Atentamente";
        $salida2 = $salida2."<br />Su salon de belleza ".$nombreEmpresa;

        $cuerpoCorreo = "<html><body> <center><img src='https://www.rinconestilista.cl/img/logo50.jpg' alt='Rincon Estilista' /></center>";
        $cuerpoCorreo = $cuerpoCorreo."<br />".$salida2;
        $cuerpoCorreo = $cuerpoCorreo."<br /></body></html>";

        $enviado = $this->Model_Reserva->enviaCorreo($correoProfesional, $cuerpoCorreo, "Gracias por trabajar con nosotros");

      }
      $data['activo'] = 'reserva';

      $this->load->view("plantilla/header", $data);
      $this->load->view("reserva/reservarealizada");
      $this->load->view("plantilla/footer");
    }



    public function reservaUsuario()
    {
      $data['activo'] = 'reservaUsuario';
      $data['titulo'] = 'Rincon estilista - Mis Reservas';
      $data['msg'] = '';



      if(!$this->session->userdata('idusuario'))
      {
        $data['activo'] = 'login';
        $data['titulo'] = 'Rincon estilista - Login';

        $this->load->view("plantilla/header", $data);
        $this->load->view("login/index");
        $this->load->view("plantilla/footer");
        return;
      }

      $data['reservas'] = $this->Model_Reserva->reservasUsuario($this->session->userdata('idusuario'));
      //echo "Numero de registros ".$data['reservas']->num_rows();

      $this->load->view("plantilla/header", $data);
      $this->load->view("reserva/reservaUsuario");
      $this->load->view("plantilla/footer");
    }


    public function anularReserva()
    {
      $idProfesional="";
      $idServicio="";
      $laHora = "";
      $idUsuario= $this->session->userdata('idusuario');
      $reserva = $this->input->post('action');
      $datosReserva = $this->Model_Reserva->datosReserva($reserva);
      foreach($datosReserva->result() as $row)
      {
        $idProfesional = $row->idProfesional;
        $fecha =  $row->fecha;
        $idServicio = $row->idServicio;
        $laHora = $row->hora;
      }

      try
      {
        if(!isset($fecha))
        {
          $data['titulo'] = 'Home';
      		$data['activo'] = 'home';
          $this->load->view("plantilla/header", $data);
          $this->load->view('home/home');
          $this->load->view("plantilla/footer");
          return;
        }

        $lasFechas = $this->Model_Reserva->nombreDia($fecha);
        $nombreDia = "";
        $fechaEspa = "";
        $laFecha = "";
        foreach ($lasFechas->result() as $row)
        {
          $nombreDia = $row->dia;
          $fechaEspa = $row->fechaEspa;
        }
        $laFecha = $this -> fechaCastellano($fechaEspa);
        $nombreServicio = $this->Model_Reserva->nombreServicio($idServicio);
        $nombreProfesional = $this->Model_Reserva->nombreDelProfesional($idProfesional);
      }
      catch (\Exception $e)
      {
        $data['msg'] = '';
        $data['activo'] = 'reservaUsuario';
        $data['titulo'] = 'Rincon estilista - Mis Reservas';
        $this->load->view("plantilla/header", $data);
        $this->load->view("reserva/reservaUsuario");
        $this->load->view("plantilla/footer");
        return;
      }

      $data['msg'] = '';

      $data['activo'] = 'reservaUsuario';
      $data['titulo'] = 'Rincon estilista - Mis Reservas';

      if(!$this->session->userdata('idusuario'))
      {
        $data['activo'] = 'login';
        $data['titulo'] = 'Rincon estilista - Login';

        $this->load->view("plantilla/header", $data);
        $this->load->view("login/index");
        $this->load->view("plantilla/footer");
        return;
      }

      if($this->Model_Reserva->anulaReservaUsuario($reserva,$idUsuario))
      {
        $data['msg'] = "Reserva numero ".$reserva." Anulada correctamente<br /><br />";

        $salida2 = "La Reserva numero ".$reserva;
        $salida2 = $salida2."<br />Para el dia ".$laFecha." a las ".$laHora;
        $salida2 = $salida2."<br />Correspondiente a un servicio de ".$nombreServicio;
        $salida2 = $salida2."<br />Fue anulada por el usuario";
        $salida2 = $salida2."<br /><br />Gracias por su preferencia";

        $cuerpoCorreo = "<html><body> <center><img src='https://www.rinconestilista.cl/img/logo50.jpg' alt='Rincon Estilista' /></center>";
        $cuerpoCorreo = $cuerpoCorreo."<br />".$salida2;
        $cuerpoCorreo = $cuerpoCorreo."<br /></body></html>";

        $correoProfesional = $this->Model_Reserva->correoDelProfesional($idProfesional);
        $correoUsuario = $this->Model_Reserva->correoDelUsuario($idUsuario);

        $enviado = $this->Model_Reserva->enviaCorreo($correoUsuario, $cuerpoCorreo, "Gracias por reservar con nosotros");
        $enviado = $this->Model_Reserva->enviaCorreo($correoProfesional, $cuerpoCorreo, "Gracias por trabajar con nosotros");

      }
      else
      { $data['msg'] = "No fue posible anular la  Reserva numero ".$reserva." <br />Por favor Contactese con nosotros para anular su reserva"; }

      $data['reservas'] = $this->Model_Reserva->reservasUsuario($this->session->userdata('idusuario'));
      //echo "Numero de registros ".$data['reservas']->num_rows();

      $this->load->view("plantilla/header", $data);
      $this->load->view("reserva/reservaUsuario");
      $this->load->view("plantilla/footer");
    }
}
?>

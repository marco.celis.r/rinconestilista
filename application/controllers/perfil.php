<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class perfil extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this -> load -> model('Model_Perfil');
    $this -> load ->helper('url');
  }

  public function empresa()
  {
    $data['titulo'] = 'Rincon Estilista - Mi Perfil';
    $data['activo'] = 'empresa';
    $data['msg'] = '';
    $this->load->view("plantilla/header", $data);
    $this->load->view("perfil/empresa");
    $this->load->view("plantilla/footer");
  }

  public function usuario()
  {
    $data['titulo'] = 'Rincon Estilista - Mi Perfil';
    $data['activo'] = 'perfil';
    $data['msg'] = '';
    $this->load->view("plantilla/header", $data);
    $this->load->view("perfil/usuario");
    $this->load->view("plantilla/footer");
  }

}
?>

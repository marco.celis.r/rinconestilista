<?php
class login extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this -> load -> model('Model_Login');
    //$this->load->library('session');
   }

   /*************************** Codificamos el string****************************************************/
   function encode($value)
   {
        if (!$value) {
            return false;
        }

        $key = sha1('EnCRypT10nK#Y!RiSRNn');
        $strLen = strlen($value);
        $keyLen = strlen($key);
        $j = 0;
        $crypttext = '';

        for ($i = 0; $i < $strLen; $i++) {
            $ordStr = ord(substr($value, $i, 1));
            if ($j == $keyLen) {
                $j = 0;
            }
            $ordKey = ord(substr($key, $j, 1));
            $j++;
            $crypttext .= strrev(base_convert(dechex($ordStr + $ordKey), 16, 36));
        }

        return $crypttext;
    }

 /******************************* Decodifica ************************************************/
 function decode($value)
 {
        if (!$value) {
            return false;
        }

        $key = sha1('EnCRypT10nK#Y!RiSRNn');
        $strLen = strlen($value);
        $keyLen = strlen($key);
        $j = 0;
        $decrypttext = '';

        for ($i = 0; $i < $strLen; $i += 2) {
            $ordStr = hexdec(base_convert(strrev(substr($value, $i, 2)), 36, 16));
            if ($j == $keyLen) {
                $j = 0;
            }
            $ordKey = ord(substr($key, $j, 1));
            $j++;
            $decrypttext .= chr($ordStr - $ordKey);
        }

        return $decrypttext;
    }

  /*************************************************************************************/
  public function index()
  {
    $data['msg'] = '';
    $data['activo'] = 'login';
    $descripcion='';
    $logo='';

    if(isset($_POST['correo']) && isset($_POST['contra']))
    {
      $i=0;
      $descripcion = "";
      $logo = "";
      $nombreFantasia="";
      $idEmpresa=0;
      $idSucursal=0;

      foreach ($this->Model_Login->login($_POST['correo'], $_POST['contra']) as $perreq)
        {
            $lo = FALSE;
            if($perreq->estado == 'Verificado'){ $lo = TRUE; }

            if($perreq->tipoUsuario == 1)
            {
                $idusua = $perreq->idUsuario;
                foreach ($this->Model_Login->empresaUsuario($idusua) as $emp)
                {
                    $idEmpresa = $emp->idEmpresa;
                    $idSucursal = $emp->idSucursal;
                    $descripcion = $emp->descripcion;
                    $logo = $emp->logo;
                    $nombreFantasia = $emp->nombreFantasia;
                }

            }

            $loguedUser = array(
               'nombreUsuario'  => $perreq->nombre,
               'email'     => $perreq->correo,
               'idusuario' => $perreq->idUsuario,
               'estado' => $perreq->estado,
               'tipo' => $perreq->tipoUsuario,
               'logged_in' => $lo,
               'idEmpresa' => $idEmpresa,
               'idSucursal' => $idSucursal,
               'descripcion' => $descripcion,
               'logo' => $logo,
               'nombreFantasia' => $nombreFantasia
             );

            if($perreq->estado <> 'Verificado')
            {
              $data['msg'] = "Bienvenido ".$perreq->nombre."<br />Se envio un correo de verificacion a ".$perreq->correo;
            }
            else
            {
              $data['msg'] = "Bienvenido ".$perreq->nombre;
            }

             $i++;
        }
        //Echo 'Total record : '.$i;


      if($i > 0)
      {
        /********* LOGIN CORRECTO **********/
        $this->session->set_userdata($loguedUser);

        $data['titulo'] = 'Rincon estilista';
        $this->load->view("plantilla/header", $data);
        $this->load->view("contactos/index");
        $this->load->view("plantilla/footer");
      }
      else
      {
        $data['titulo'] = 'Rincon estilista - Login(e)';
        $data['msg'] = 'Las credenciales ingresadas no coinciden';
        $this->load->view("plantilla/header", $data);
        $this->load->view("login/index");
        $this->load->view("plantilla/footer");
      }
    }
    else
    {
      $data['activo'] = 'login';
      $data['titulo'] = 'Rincon estilista - Login.';
      $this->load->view("plantilla/header", $data);
      $this->load->view("login/index");
      $this->load->view("plantilla/footer");
    }
  }


  public function recuperar()
  {
    $data['activo'] = 'login';
    $data['titulo'] = 'Rincon estilista - Login.';
    $data['msg'] = '';
    $this->load->view("plantilla/header", $data);
    $this->load->view("login/recuperar");
    $this->load->view("plantilla/footer");
  }


  public function recuperarClave()
  {
    $data['msg'] = "";
    $envioCorreo = $this->input->post('correo');

    if(!empty($envioCorreo))
    {
      $existe = $this->Model_Login->existeCorreo($envioCorreo);
      if($existe > 0)
      {
        $encrypted = $base_64 = base64_encode($envioCorreo);

        $asunto = "Solicitud de cambio de clave";
        $mensaje = "Hemos recivido su solicitud de cambio de clave para nuestro sitio
                    <br />Para recuperar su clave, por favo de clic al siguiente enlace
                    <br /><br /><a href='".base_url()."login/modificarClave/".$encrypted."'>Cambio de clave</a>
                    <br /><br />Si usted no ha solicitado este cambio de
                    <br />por favor omita este correo
                    <br /><br />Gracias por trabajar con nosotros";

        $cuerpoCorreo = "<html><body> <center><img src='https://www.rinconestilista.cl/img/logo50.jpg' alt='Rincon Estilista' /></center>";
        $cuerpoCorreo = $cuerpoCorreo."<br />".$mensaje;
        $cuerpoCorreo = $cuerpoCorreo."<br /></body></html>";

        $enviado = $this->Model_Login->enviaCorreoLogin($envioCorreo, $cuerpoCorreo, $asunto);

        $data['msg'] = "Le hemos enviado un correo, si aun no lo recibe
                        <br />Por favor, revise su casilla de spam o correo no deseado";
      }
      else {
        $data['msg'] = "El correo indicado no se encuentra registrado
                        <br />Debe ser un usuario regitrado para cambiar su clave";
      }
    }


    $data['activo'] = 'login';
    $data['titulo'] = 'Rincon Estilista - Recuparar clave';
    $this->load->view("plantilla/header", $data);
    $this->load->view("login/recuperar");
    $this->load->view("plantilla/footer");
  }



  public function modificarClave($id)
  {
    if(empty($id))
    {
      $data['msg'] = "Debe ser un usuario regitrado
                      <br />Para cambiar su clave";
      $data['activo'] = 'login';
      $data['titulo'] = 'Rincon Estilista - Recuparar clave';
      $this->load->view("plantilla/header", $data);
      $this->load->view("login/modificar");
      $this->load->view("plantilla/footer");
      return;
    }

    $desencrypted = base64_decode($id);
    $data['correo'] = $desencrypted;


    $data['msg'] = "";
    $data['activo'] = 'login';
    $data['titulo'] = 'Rincon Estilista - Recuparar clave';
    $this->load->view("plantilla/header", $data);
    $this->load->view("login/modificar");
    $this->load->view("plantilla/footer");

  }


  function actualizaLaClave()
  {

      $clave1 = $this->input->post('contra');
      $clave2 = $this->input->post('contra2');
      $correo = $this->input->post('elCorreo');
      $data['correo'] = $correo;

      if(empty($clave1) || empty($clave2))
      {
        $data['msg'] = "Debe ingresar un valor para las claves
                        <br />El valor de los campos debe ser identico";

        $data['activo'] = 'login';
        $data['titulo'] = 'Rincon Estilista - Cambio de clave';
        $this->load->view("plantilla/header", $data);
        $this->load->view("login/modificar");
        $this->load->view("plantilla/footer");
        return;
      }

      if($clave1  <> $clave2)
      {
        $data['msg'] = "El valor de los dos campos debe ser identico";

        $data['activo'] = 'login';
        $data['titulo'] = 'Rincon Estilista - Cambio de clave';
        $this->load->view("plantilla/header", $data);
        $this->load->view("login/modificar");
        $this->load->view("plantilla/footer");
        return;
      }

      if ($this->Model_Login->updateClave($clave1, $correo))
      { $data['msg'] = "Su clave ha sido actualizada correctamente"; }
      else
      { $data['msg'] = "No fue posible actualizar su clave<br />Por favor contactese con nosotros."; }


      $data['activo'] = 'login';
      $data['titulo'] = 'Rincon Estilista - Clave actualizada';
      $this->load->view("plantilla/header", $data);
      $this->load->view("login/modificar");
      $this->load->view("plantilla/footer");
  }
}
 ?>

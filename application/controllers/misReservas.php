<?php
class misReservas extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this -> load -> model('Model_misReservas');
    $this -> load -> model('Model_Reserva');
   }

   public function enviaEstadoReserva($idReserva, $estado)
   {
     $datosReserva = $this->Model_misReservas->datosReserva($idReserva);
     foreach ($datosReserva->result() as $row)
     {
       $fecha = $row->fecha;
       $hora = $row->hora;
       $nombreServicio = $row->descripcion;
       $correoP  = $row->correoP;
       $correoU  = $row->correU;
       $idProfesional = $row->idProfesional;
       $idUsuario = $row->idUsuario;
     }

     $salida2 = "La Reserva numero ".$idReserva;
     $salida2 = $salida2."<br />Para el dia ".$fecha." a las ".$hora;
     $salida2 = $salida2."<br />Correspondiente a un servicio de ".$nombreServicio;
     $salida2 = $salida2."<br />Fue ".$estado." por el profesional";
     $salida2 = $salida2."<br /><br />Gracias por su preferencia";

     $cuerpoCorreo = "<html><body> <center><img src='https://www.rinconestilista.cl/img/logo50.jpg' alt='Rincon Estilista' /></center>";
     $cuerpoCorreo = $cuerpoCorreo."<br />".$salida2;
     $cuerpoCorreo = $cuerpoCorreo."<br /></body></html>";

     $correoProfesional = $this->Model_Reserva->correoDelProfesional($idProfesional);
     $correoUsuario = $this->Model_Reserva->correoDelUsuario($idUsuario);

     $enviado = $this->Model_Reserva->enviaCorreo($correoU, $cuerpoCorreo, "Gracias por reservar con nosotros");
     $enviado = $this->Model_Reserva->enviaCorreo($correoP, $cuerpoCorreo, "Gracias por trabajar con nosotros");

     return true;
   }

  public function index()
  {
    $data['titulo'] = 'Rincon estilista - Mis Reservas';
    $data['activo'] = 'misReservas';
    $data['msg'] = '';

    $idUsuario= $this->session->userdata('idusuario');

    $anula=0;
    $anula = intval($this->input->post('anula'));

    if($anula > 0)
    {
      if($this->Model_misReservas->anulaReservaUsuario($anula,$idUsuario))
      {
        $data['msg'] = 'Reserva anulada correctamente';
        $datosReserva = $this->enviaEstadoReserva($anula, 'Anulada');
      }
      else
      {
          $data['msg'] = 'No fue posible anular esta reserva';
      }
    }

    $recepcion=0;
    $recepcion = intval($this->input->post('recepciona'));
    if($recepcion > 0)
    {
      if($this->Model_misReservas->recepcionaReservaProfesional($recepcion,$idUsuario))
      {
        $data['msg'] = 'Reserva recepcionada correctamente';
        $datosReserva = $this->enviaEstadoReserva($recepcion, 'Recepcionada');
      }
      else
      {
          $data['msg'] = 'No fue posible recepcionar esta reserva';
      }
    }

    $idUsuario= $this->session->userdata('idusuario');
    $data['reservas'] = $this->Model_misReservas->misReserva($idUsuario);

    $this->load->view("plantilla/header", $data);
    $this->load->view("misReservas/index");
    $this->load->view("plantilla/footer");
  }

  /*function misReservas()
  {
    $this->load->view("plantilla/header", $data);
    $this->load->view("misReservas/index");
    $this->load->view("plantilla/footer");
  }*/
}
 ?>

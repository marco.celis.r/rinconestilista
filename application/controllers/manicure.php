<?php
class manicure extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
   }

  public function index()
  {
    $data['activo'] = 'empresa';
    $data['msg'] = '';
    $data['titulo'] = 'Rincon estilista - Manicure.';

    $this->load->view("plantilla/header", $data);
    $this->load->view("manicure/index");
    $this->load->view("plantilla/footer");

  }
}
 ?>

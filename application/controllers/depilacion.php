<?php
class depilacion extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
   }

  public function index()
  {

    $data['activo'] = 'empresa';
    $data['msg'] = '';
    $data['titulo'] = 'Rincon estilista - Depilacion.';

    $this->load->view("plantilla/header", $data);
    $this->load->view("depilacion/index");
    $this->load->view("plantilla/footer");

  }
}
 ?>

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

  public function __construct()
  {

    parent::__construct();
    $this->load->helper('url');
    $this->load->model('User_model');
  }

  public function index()
  {
    $data['activo'] = 'servicio';
    $data['titulo'] = 'Agenda de profesionales';
    $data['msg'] = '';

    $this->load->view('plantilla/header', $data);
    $this->load->view('user/index');
    $this->load->view('plantilla/footer');

  }

  public function userList()
  {
    $idEmpresa= $this->session->userdata('idEmpresa');
    $idSucursal= $this->session->userdata('idSucursal');
    $postData = $this->input->post();
    $data = $this->User_model->getUsers($postData,$idEmpresa,$idSucursal);
    echo json_encode($data);
  }

  public function serviceList()
  {
    $idEmpresa= $this->session->userdata('idEmpresa');
    $idSucursal= $this->session->userdata('idSucursal');
    $postData = $this->input->post();
    $data = $this->User_model->getService($postData,$idEmpresa,$idSucursal);
    echo json_encode($data);
  }

  public function generarAgenda()
  {
    $data['activo'] = 'servicio';
    $sess_id = $this->session->userdata('idEmpresa');
    $sess_sucursal = $this->session->userdata('idSucursal');

    if(empty($sess_id))
    {
      $data['titulo'] = 'Login';
      $data['msg'] = '';

      $this->load->view('plantilla/header', $data);
      $this->load->view('login/index');
      $this->load->view('plantilla/footer');
      return;
    }

    $data['msg'] ='';

    $this->form_validation->set_rules('autouser','Profesional','required|trim');
    $this->form_validation->set_rules('autoservice','Servicio','required|trim');
    $this->form_validation->set_rules('horaInicio','Hora de inicio de la jornada','required');
    $this->form_validation->set_rules('horaFin','Hora de termino de la jornada','required');
    $this->form_validation->set_rules('frecuencia','Frecuencia de atencion','required|numeric|less_than_equal_to[180]');

    $losDias = $this->input->post('dias');
    $dia = '';

    if(strlen(trim($losDias)) < 1)
    {
      $lun = $this->input->post('lunes');
      $dia = $lun;
      $mar = $this->input->post('martes');
      if (strlen(trim($dia)) > 0 && strlen(trim($mar)) > 0) { $dia=$dia.',';}
      $dia = $dia.$mar;
      $mie = $this->input->post('miercoles');
      if (strlen(trim($dia)) > 0  && strlen(trim($mie)) > 0) { $dia=$dia.',';}
      $dia = $dia.$mie;
      $jue = $this->input->post('jueves');
      if (strlen(trim($dia)) > 0  && strlen(trim($jue)) > 0) { $dia=$dia.',';}
      $dia = $dia.$jue;
      $vie = $this->input->post('viernes');
      if (strlen(trim($dia)) > 0  && strlen(trim($vie)) > 0) { $dia=$dia.',';}
      $dia = $dia.$vie;
      $sab = $this->input->post('sabado');
      if (strlen(trim($dia)) > 0  && strlen(trim($sab)) > 0) { $dia=$dia.',';}
      $dia = $dia.$sab;
      $dom = $this->input->post('domingo');
      if (strlen(trim($dia)) > 0  && strlen(trim($dom)) > 0) { $dia=$dia.',';}
      $dia = $dia.$dom;
    } else {
      $dia = $losDias;
    }
    /*
    evalusraemos si la cantidad de hjoras de la agenda es mayor
    al minimo permitido (180 minutos o 3 horas)
    */
    $horaBuena = FALSE;
    $time1 = $this->input->post('horaInicio');
    $time2 = $this->input->post('horaFin');

    if(strtotime($time2) > strtotime($time1))
    {
        $hourdiff = round((strtotime($time2) - strtotime($time1))/3600, 1);

        if($hourdiff >= MINIMO_HORAS)
        { $horaBuena = TRUE; }
    }

    if($this -> form_validation -> run() == FALSE
        || intval($this->input->post('frecuencia')) > MAX_FRECUENCIA
        || $horaBuena == FALSE)
    {
      $data['titulo'] = 'Agenda de profesionales';
      if(strlen(trim($dia)) < 1)
      {
          $data['msg'] = 'Debe seleccionar los dias de atencion de la agenda';
      }
      if(intval($this->input->post('frecuencia')) > MAX_FRECUENCIA)
      {
          if(strlen(trim($data['msg'])) > 1) { $data['msg'] = $data['msg'].'<br /'; }
          $data['msg'] = $data['msg'].'La frecuencia no puede superar los '.MAX_FRECUENCIA.' minutos';
      }
      if($horaBuena == FALSE)
      {
          if(strlen(trim($data['msg'])) > 1) { $data['msg'] = $data['msg'].'<br /'; }
          $data['msg'] = $data['msg'].'El rango horario de atencion debe ser superios a '.MINIMO_HORAS.' horas';
      }

      $this->load->view('plantilla/header', $data);
      $this->load->view('user/index');
      $this->load->view('plantilla/footer');

      return;
    }

    /******************************* Si el profesional o el servicio fuero ingresados a mano ******/

    $autouser = $this->input->post('userid');
    $autoservice = $this->input->post('serviceid');

    if(empty($autouser))
    {
      $data['titulo'] = 'Rincon Estilista - Agenda';
      $data['titulo'] = 'empresa';
      $data['msg'] = $data['msg'].'El profesional ingresado no se encuentra registrado
                                    <br />Antes de generar su agenda debe crear al profesional';
      $this->load->view('plantilla/header', $data);
      $this->load->view('user/index');
      $this->load->view('plantilla/footer');
      return;
    }

    if(empty($autoservice))
    {
      $data['titulo'] = 'Rincon Estilista - Agenda';
      $data['titulo'] = 'empresa';
      $data['msg'] = $data['msg'].'El Servicio ingresado no se encuentra registrado
                                    <br />Antes de generar su agenda debe crear el Servicio';
      $this->load->view('plantilla/header', $data);
      $this->load->view('user/index');
      $this->load->view('plantilla/footer');
      return;
    }


    /******************************* fin validacion de ingresos a mano ***************************/

/*************************************************** GENERACION DE AGENDA ******************************/
    $action = $this->input->post('action');
    if($action == 'generar')
    {

      $fecha = '2021-06-10';
      $horaIni = $this->input->post('horaInicio');
      $horaFin = $this->input->post('horaFin');
      $frecuencia =  $this->input->post('frecuencia');

      $salida = "<table>";
      foreach ($this->User_model->getAgenda($frecuencia, $fecha,$horaIni,$horaFin) as $emp)
      {
          $hora = $emp['horasub']; //$emp->horasub;
          $salida = $salida."<tr><td><input type='checkbox' name='horaSeleccionada[]' id='horaSeleccionada[]' value=".$hora."><label for='horaSeleccionada'>".$hora."</label></td></tr>";
      }
      $salida = $salida."</table>";

      $data['titulo'] = 'Agenda de profesionales';
      $data['agenda'] = TRUE;
      $data['horas'] = $salida;

      /* mantenemos los datos que venian en la generacion*/
      $data['autouser'] = $this->input->post('autouser');
      $data['autoservice'] = $this->input->post('autoservice');
      $data['userid'] = $this->input->post('userid');
      $data['serviceid'] = $this->input->post('serviceid');
      $data['dias'] = $dia;
      $data['guardarAgenda'] = 1;

      if(strlen(trim($lun)) > 0) { $data['lunes'] = 'checked disabled'; } else { $data['lunes'] = 'disabled'; }
      if(strlen(trim($mar)) > 0) { $data['martes'] = 'checked disabled'; } else { $data['martes'] = 'disabled'; }
      if(strlen(trim($mie)) > 0) { $data['miercoles'] = 'checked disabled'; } else { $data['miercoles'] = 'disabled'; }
      if(strlen(trim($jue)) > 0) { $data['jueves'] = 'checked disabled'; } else { $data['jueves'] = 'disabled'; }
      if(strlen(trim($vie)) > 0) { $data['viernes'] = 'checked disabled'; } else { $data['viernes'] = 'disabled'; }
      if(strlen(trim($sab)) > 0) { $data['sabado'] = 'checked disabled'; } else { $data['sabado'] = 'disabled'; }
      if(strlen(trim($dom)) > 0) { $data['domingo'] = 'checked disabled'; } else { $data['domingo'] = 'disabled'; }


      $data['horaInicio'] = $this->input->post('horaInicio');
      $data['horaFin'] = $this->input->post('horaFin');
      $data['frecuencia'] =  $this->input->post('frecuencia');
      /***************************************************/

      $this->load->view("plantilla/header", $data);
      $this->load->view('user/index');
      $this->load->view("plantilla/footer");
      return;
    }

/*************************************************** GUARDAR INFORMACION DE LA AGENDA ******************************/
    if($action == 'guardar')
    {
      $data['titulo'] = 'Agenda de profesionales';

      if(!isset($_POST['horaSeleccionada']))
      {
        $data['activo'] = 'empresa';
        $data['msg'] = "Debe seleccionar un periodo de descanso para el profesional
                        <br />Puede seleccionar uno o mas periodos de descanso";
        $this->load->view("plantilla/header", $data);
        $this->load->view('user/index');
        $this->load->view("plantilla/footer");
        return;
      }
      $horas = '';
      $checkbox = $_POST['horaSeleccionada'];
      for($i=0;$i<count($checkbox);$i++)
      {
		     $EstaHora = $checkbox[$i];
         $horas = $horas."'".$EstaHora."',";
       }
       $horas = substr($horas, 0, -1);
       //echo '<br />Hora: '.$horas;

       $horarioProfesional = array(
         'idServicio' => $this->input->post('serviceid'),
         'idProfesional' => $this->input->post('userid'),
         'idEmpresa' => $sess_id,
         'idSucursal' => $sess_sucursal,
         'vigencia' => 'S',
         'diasAtencion' => $this->input->post('dias'),
         'horaInicio' => $this->input->post('horaInicio'),
         'horaFin' => $this->input->post('horaFin'),
         'frecuencia' => $this->input->post('frecuencia'),
         'descanso' =>$horas
        );

        if ($this -> User_model -> existeAgenda($sess_id, $sess_sucursal, $this->input->post('serviceid'), $this->input->post('userid')))
        {
          $data['msg'] = "El profesional ya tiene registrada una agenda para este servicio<br />Para generar una nueva agenda, primero anule la agenda existente";
          $this->load->view("plantilla/header", $data);
          $this->load->view('user/index');
          $this->load->view("plantilla/footer");
          return;
        }

        try
        {
          if($this -> User_model -> insertar($horarioProfesional))
          {
              $last_agenda = $this->db->insert_id();
              $data['msg'] = "Agenda registrada correctamente: ".$last_agenda;
          }
        }
        catch (\Exception $e)
        {
          $newuser = array(
             'error_code'  => 404,
             'merror_mensaje'  => 'No fue posible regitrar al usuario',
             'logged_in' => FALSE
           );

           $data['msg'] = "Ocurrio un error al registrar la agenda: ".$e->getMessage();
        }

        $this->load->view("plantilla/header", $data);
        $this->load->view('user/index');
        $this->load->view("plantilla/footer");
        return;
    }

  }


}

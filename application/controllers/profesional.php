<?php
class profesional extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this -> load -> model('Model_Profesional');
  }

  public function index()
  {
    $data['activo'] = 'empresa';
    $data['titulo'] = 'Rincon Estilista - Profesionales';
    $data['msg'] = '';
    $this->load->view("plantilla/header", $data);
    $this->load->view("profesional/index");
    $this->load->view("plantilla/footer");
  }

  public function agregarprofesional()
  {
    $data['activo'] = 'empresa';
    $this->form_validation->set_rules('nnombre','Nombre','required');
    $this->form_validation->set_rules('ntelefono','Telefono','required|numeric|trim|min_length[9]|max_length[10]');
    $this->form_validation->set_rules('ncorreo','Direccion de Correo','required|trim|valid_email');

    if($this -> form_validation -> run() == FALSE)
    {
      $data['titulo'] = 'Rincon Estilista - Profesionales';
      $data['msg'] = '';
      $this->load->view("plantilla/header", $data);
      $this->load->view("profesional/index");
      $this->load->view("plantilla/footer");
      return;
    }
    else
    {
      if($this -> Model_Profesional -> existeProfesional($this->input->post('ncorreo'), $this->input->post('nnombre')))
      {
        $data['titulo'] = 'Rincon Estilista - Profesionales';
        $data['msg'] = 'El profesional ya se encuentra registrado';
        $this->load->view("plantilla/header", $data);
        $this->load->view("profesional/index");
        $this->load->view("plantilla/footer");
        return;
      }

      $idEmpresa= $this->session->userdata('idEmpresa');
      $idSucursal= $this->session->userdata('idSucursal');
      $data = array(
          'nombre'=>$this->input->post('nnombre'),
          'correo'=>$this->input->post('ncorreo'),
          'telefono'=>$this->input->post('ntelefono'),
          'idEmpresa'=>$idEmpresa,
          'idSucursal'=>$idSucursal
      );

      $this -> Model_Profesional -> insertar($data);
      $last_usuario = $this->db->insert_id();

      if($last_usuario > 0)
      {
        $data['activo'] = 'empresa';
        $data['titulo'] = 'Rincon Estilista - Profesionales';
        $data['msg'] = 'El profesional ha sido registrado exitosamente ('.$last_usuario.')';
        $this->load->view("plantilla/header", $data);
        $this->load->view("profesional/index");
        $this->load->view("plantilla/footer");
        return;
      }

    }
  }

}
 ?>

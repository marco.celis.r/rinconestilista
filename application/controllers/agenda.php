<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class agenda extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this -> load -> model('Model_Agenda');
    $this -> load -> model('Model_Profesional');
    $this -> load ->helper('url');
  }

  public function index()
  {
    $data['titulo'] = 'Rincon Estilista - Agenda de profesionales';
    $data['activo'] = 'empresa';
    $data['msg'] = '';
    $this->load->view("plantilla/header", $data);
    $this->load->view("agenda/index");
    $this->load->view("plantilla/footer");
  }

  public function agregaragenda()
  {
    $data['activo'] = 'empresa';
    $this->form_validation->set_rules('nnombre','Nombre','required|max_length[100]');

    if (!empty($this->input->post("goto1btn")))
    {
      $data['msg'] = 'Boton 1';
    }
    else if (!empty($this->input->post("goto2btn")))
    {
      $data['msg'] = 'Boton 2';
    }
    else if (!empty($this->input->post("buscarNombre")))
    {
      $data['msg'] = 'buscando nombre';
      echo $this->Model_Agenda->fetch_data($this->uri->segment(3));
    }
    else
    {
      $data['msg'] = 'Otro boton';
    }

    $data['titulo'] = 'Rincon Estilista - Agenda de profesionales';
    $data['msg'] = '';
    $this->load->view("plantilla/header", $data);
    $this->load->view("agenda/index");
    $this->load->view("plantilla/footer");

    if($this -> form_validation -> run() == FALSE)
    {/*
      $data['titulo'] = 'Rincon Estilista - Agenda';
      $data['msg'] = 'No se pudo generar la Agenda';
      $this->load->view("plantilla/header", $data);
      $this->load->view("agenda/index");
      $this->load->view("plantilla/footer");
    */}
  }

}
 ?>

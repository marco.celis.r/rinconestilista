<?php
class barberia extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
   }

  public function index()
  {

    $data['activo'] = 'empresa';
    $data['msg'] = '';
    $data['titulo'] = 'Rincon estilista - Barberia.';

    $this->load->view("plantilla/header", $data);
    $this->load->view("barberia/index");
    $this->load->view("plantilla/footer");

  }
}
 ?>

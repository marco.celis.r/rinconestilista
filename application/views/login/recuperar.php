<style>
  #intro {
    background-image: url(https://blog.agendapro.com/hubfs/centro%20de%20belleza%20vac%C3%ADo.png);
    height: 90vh;
  }

  /* Height for devices larger than 576px */
  @media (min-width: 992px) {
    #intro {
      margin-top: -58.59px;
    }
  }
</style>

<br><br>
<?php echo form_open(base_url() . 'login/recuperarClave'); ?>
<div id="intro">
  <div class="Container">
    <div class="row justify-content-center">
      <div class="col-xl-5 col-md-8">
        <br><br><br>
        <div class="bg-white  rounded-5 shadow-5-strong p-5">
          <center>
            <h3>Recuperar Contraseña</h3>
            <br>
          </center>

          <div class="col-4">
            <?php
            echo validation_errors();
            echo $msg;
            ?>
          </div>


          <center>


            <div class="ContentForm">
              <div class="input-group input-group-lg">
                <span class="input-group-text" id="basic-addon1"><i class="bi bi-at"></i></span>
                <input type="email" class="form-control" name="correo" placeholder="Correo" id="correo" aria-describedby="sizing-addon1" minlength="5" maxlength="30" required>
              </div>
              <br />

              <button class="btn btn-lg btn-primary btn-block btn-signin" id="IngresoLog" type="submit">Enviar</button>
            </div>

          </center>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  (function() {
    'use strict'

    var forms = document.querySelectorAll('.needs-validation')

    Array.prototype.slice.call(forms)
      .forEach(function(form) {
        form.addEventListener('submit', function(event) {
          if (!form.checkValidity()) {
            event.preventDefault()
            event.stopPropagation()
          }

          form.classList.add('was-validated')
        }, false)
      })
  })()
</script>


<?php echo form_close(); ?>
 <style>
   #intro {
     background-image: url(https://blog.agendapro.com/hubfs/centro%20de%20belleza%20vac%C3%ADo.png);
     height: 90vh;
   }

   /* Height for devices larger than 576px */
   @media (min-width: 992px) {
     #intro {
       margin-top: -58.59px;
     }
   }
 </style>
 <br>
 <br>
 <div id="intro">
   <div class="Container">
     <div class="row justify-content-center">
       <div class="col-xl-5 col-md-8">
         <br>
         <div class="bg-white  rounded-5 shadow-5-strong p-5">
           <center>
             <h3>Login</h3>
             <br>
           </center>
           <?php echo form_open(base_url() . 'login'); ?>

           <div class="Icon">
             <span class="glyphicon glyphicon-user"></span>
           </div>
           <div class="ContentForm">
             <div class="input-group input-group-lg">
               <span class="input-group-text" id="basic-addon1"><i class="bi bi-at"></i></span>
               <input type="email" class="form-control" name="correo" placeholder="Correo" id="correo" aria-describedby="sizing-addon1" required>
             </div>
             <br>
             <div class="input-group input-group-lg">
               <span class="input-group-text" id="basic-addon1"><i class="bi bi-key"></i></span>
               <input type="password" name="contra" class="form-control" placeholder="******" aria-describedby="sizing-addon1" required>
             </div>
             <br>
             <div class="opcioncontra">
               <a href="<?php echo base_url() . 'login/recuperar'; ?>">Olvidaste tu contrase&ntilde;a?</a>
             </div>
             <br>
		
               <?php
                echo validation_errors();
                echo $msg;
                ?>
            
		   <br><br>
             <button class="btn btn-lg btn-primary btn-block btn-signin" id="IngresoLog" type="submit">Entrar</button>

           </div>


           <?php echo form_close(); ?>

         </div>
       </div>
     </div>
   </div>
 </div>
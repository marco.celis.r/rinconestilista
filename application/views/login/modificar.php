<?php echo form_open(base_url().'login/actualizaLaClave'); ?>
  <div class="Container">
    <div class="row">
      <div class="col-md-12 col-md_offset-4">
        <center>
          <h1>Cambio de clave</h1>
          <br />
          <input type"hidden" name="elCorreo" id="elCorreo" style="display:none;" value="<?php echo $correo; ?>" readonly>
          <div class="ContentForm">

            <div class="input-group input-group-lg">
              <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-lock"></i></span>
              <center><label>Clave</label></center>
            </div>
            <div class="input-group input-group-lg">
              <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-lock"></i></span>
              <input type="password" name="contra" id="contra" maxlength="15" minlength="6" class="form-control" placeholder="******" aria-describedby="sizing-addon1" required>
            </div>

            <br />
            <div class="input-group input-group-lg">
              <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-lock"></i></span>
              <center><label>Repita la Clave</label></center>
            </div>
            <div class="input-group input-group-lg">
              <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-lock"></i></span>
              <input type="password" name="contra2" id="contra2" maxlength="15" minlength="6" class="form-control" placeholder="******" aria-describedby="sizing-addon1" required>
            </div>

            <br />
            <br />
            <button class="btn btn-lg btn-primary btn-block btn-signin" id="action" name="action" value="modificarClave" type="submit">Enviar</button>
          </div>

          <div class="row">
            <div class="col-4">
              <?php
                echo validation_errors();
                echo $msg;
                ?>
            </div>
          </div>

        </center>
      </div>
    </div>
  </div>

  <script type="text/javascript">
  (function () {
  'use strict'

  var forms = document.querySelectorAll('.needs-validation')

  Array.prototype.slice.call(forms)
    .forEach(function (form) {
      form.addEventListener('submit', function (event) {
        if (!form.checkValidity()) {
          event.preventDefault()
          event.stopPropagation()
        }

        form.classList.add('was-validated')
      }, false)
    })
  })()

</script>


  <?php echo form_close(); ?>

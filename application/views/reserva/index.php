<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css">
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>

<style type="text/css">

  #intro {
    background-image: url();
    background-color: #8b9386;
  }

  /* Height for devices larger than 576px */
  @media (min-width: 992px) {
    #intro {
      margin-top: -58.59px;
    }
  }
</style>


<?php if (isset($noVerificado)) {
  echo $noVerificado;
}
?>
<br /><br /><br />
<div id="intro">
  <div class="Container">
    <div class="row justify-content-center">
      <div class="col-xl-5 col-md-8">
        <br>
        <div class="bg-white  rounded-5 shadow-5-strong p-5">
          <center>
            <h3>Reserva de Horas</h3>
          </center>
          <section class="py-5">
            <?php if (isset($idEmpresa) && isset($idSucursal)) { ?>
              <input type="hidden" id="hidEmpresa" name="hidEmpresa" value="<?php echo $idEmpresa; ?>" />
              <input type="hidden" id="hidSucursal" name="hidSucursal" value="<?php echo $idSucursal; ?>" />
            <?php } ?>
            <div class="container px-4 px-lg-5 mt-5">

              <?php echo form_open_multipart(base_url() . 'servicio/agregarservicio'); ?>

              <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
                <?php if (isset($results)) { ?>

                  <?php foreach ($results as $data) { ?>
                    <div class="col mb-5">
                      <div class="card h-100">
                        <!-- logo empresa-->
                        <?php
                        if (!isset($noVerificado)) { ?>
                          <a href='<?php echo base_url() ?>reserva/empresaSucursal?empresa=<?php echo $data->idEmpresa; ?>&sucursal=<?php echo $data->idSucursal; ?>'>
                          <?php } ?>
                          <img src="<?php echo $data->logo ?>" alt="<?php echo $data->nombreFantasia ?>" width="250" height="200">
                          <?php if (!isset($noVerificado)) { ?>
                          </a>
                        <?php } ?>

                        <div class="card-body p-4">
                          <div class="text-center">
                            <!-- nombre empresa-->
                            <td bgcolor="#cccccc" align="center"><?php echo $data->nombreFantasia ?></td>
                            <!-- descripcion-->
                            <td bgcolor="#cccccc" align="center"><?php echo $data->descripcion ?></td>
                          </div>
                        </div>
                      </div>
                    </div>


                  <?php } ?>

          </section>

        <?php } else { ?>
          <div>
            <?php if (!isset($buscar)) {
                    echo 'No se encontraron salones disponibles.';
                  } ?>
          </div>
        <?php } ?>

        <?php if (isset($links)) { ?>
          <?php echo "<center>".$links."</center>"; } ?>
        </div>


        <br />

        <div class="row">
          <div class="col-4">
            <?php echo validation_errors(); ?>
          </div>
        </div>

        <?php
        echo form_close();
        ?>
      </div>

      <?php if (isset($nombreFantasia)) { ?>

        <div class="Container">
          <?php echo form_open_multipart(base_url() . 'reserva/reservaServicio');


          echo "<br />Nombre: " . $nombreFantasia;
          echo "<br />Descripcion: " . $descripcion;
          echo "<br /><img src='" . $logo . "' alt='" . $nombreFantasia . "' style='max-width: 10%;width: 10%'>";
          ?>
          <br /><br />
          <label for="servicios">Servicio: </label>
          <select class="form-control select2" name="servicios" id="servicios">
            <option value="0">Selecciona el servicio que buscas</option>
            <?php
            if (!empty($servicios)) {
              foreach ($servicios as $row) {
                echo '<option value="' . $row['idServicio'] . '">' . $row['nombre'] . '</option>';
              }
            } else {
              echo '<option value="0">No se encontraron servicios</option>';
            }
            ?>
          </select>

          <br />


          <div class="form-group">
            <label for="name1">Selecciona un profesional</label>
            <select id="profesionalId" class="form-control" name="profesionalId" required>
              <option value="">-- SELECCIONE --</option>
            </select>
          </div>

          <div class="form-group" id="divAgenda">
          </div>

          <?php

          echo form_close();
          ?>
        </div>
      <?php
      }

      if (isset($msg)) {
        echo "<br />" . $msg;
      }
      ?>

      <br>

    </div>
  </div>
</div>
</div>
<br>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $("#servicios").change(function() {
      $.get("<?php echo base_url(); ?>reserva/buscaServicios", "servicioId=" + $("#servicios").val() + "&empresaId=" + $("#hidEmpresa").val() + "&sucursalId=" + $("#hidSucursal").val(), function(data) {
        $("#profesionalId").html(data);
        console.log(data);
      });
    });

    $("#profesionalId").change(function() {
      $.get("<?php echo base_url(); ?>reserva/buscaAgenda", "profesionalId=" + $("#profesionalId").val() + "&servicioId=" + $("#servicios").val() + "&empresaId=" + $("#hidEmpresa").val() + "&sucursalId=" + $("#hidSucursal").val(), function(data) {
        $("#divAgenda").html(data);
        console.log(data);
      });
    });

  });
</script>

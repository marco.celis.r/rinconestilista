<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
  $(document).ready(function(){
  $('.slectOne').on('change', function() {
     $('.slectOne').not(this).prop('checked', false);
     $('#result').html($(this).data( "id" ));
     if($(this).is(":checked"))
     	$('#result').html($(this).data( "id" ));
     else
     	$('#result').html('Por favor, reserve la hora de su atencion');

      document.getElementById('btSubmit').disabled = !$(this).is(":checked");
  });
  });
</script>

<style>
  table {
    font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
    font-size: 12px;
    margin: 45px;
    width: 480px;
    text-align: left;
    border-collapse: collapse; }

  th {
    font-size: 13px;
    font-weight: normal;
    padding: 8px;
    background: #b9c9fe;
    border-top: 4px solid #aabcfe;
    border-bottom: 1px solid #fff;
    color: #039; }

  td {
    padding: 8px;
    background: #e8edff;
    border-bottom: 1px solid #fff;
    color: #669;
    border-top: 1px solid transparent; }

  tr:hover

  td {
    background: #d0dafd;
    color: #339; }

	#intro {
        background-image: url();
        background-color: #8b9386;
    }

    /* Height for devices larger than 576px */
    @media (min-width: 992px) {
        #intro {
            margin-top: -58.59px;
        }
    }
</style>

<br />
<div id="intro">
   <div class="Container">
     <div class="row justify-content-center">
       <div class="col-xl-5 col-md-8">
         <br>
         <div class="bg-white  rounded-5 shadow-5-strong p-5">
<center><h3>Reserva de Horas</h3></center>

  <?php echo form_open_multipart(base_url().'reserva/realizarReserva');

  if (isset($horas))
  {
    echo "<br>".$estaReserva;
    ?>
    <br>
    <span id="result"></span>
   <br><br>
    <input type="submit" value="Reserva mi atencion" id="btSubmit" name="btSubmit"disabled>
    <br>
    <table><tr> <th>Horas disponibles</th></tr>

    <?php
    foreach ($horas->result() as $row)
    {
      $Existe = $this->Model_Reserva->existeReserva($hidEmpresa1
            , $hidSucursal1
            , $hidServicio
            , $hidProfesional, $hidFecha
            , $row->horasub
          );

      if(intval($Existe)==0)
      {
        $fechaReserva = $hidFecha." ".$row->horasub;
        $Existe = $this->Model_Reserva->horaValida($fechaReserva);
        if($Existe == 0)
        {
            echo "<tr><td><label><input type='checkbox' id='horaReserva' name='horaReserva'class='slectOne' value='".$row->horasub."' data-id='A las ".$row->horasub."'/> ".$row->horasub."</label></td></tr>";
        }
        else
        {
          echo "<tr><td><label><input type='checkbox' id='horaReserva' name='horaReserva'class='slectOne' value='".$row->horasub."' data-id='A las ".$row->horasub."' disabled/> ".$row->horasub." </label></td></tr>";
        }
      }
      else
      {
        echo "<tr><td><label><input type='checkbox' id='horaReserva' name='horaReserva'class='slectOne' value='".$row->horasub."' data-id='A las ".$row->horasub."' disabled/> ".$row->horasub." - Reservada</label></td></tr>";
      }

    }
    ?></table><?php
  }
  ?>
  <div class="row">
    <div class="col-4">
      <?php echo validation_errors(); ?>
      <input type='hidden' id='idSucursal' name='idSucursal' value='<?php echo $hidSucursal1;  ?>' />
      <input type='hidden' id='idServicio' name='idServicio' value='<?php echo $hidServicio;  ?>' />
      <input type='hidden' id='idProfesional' name='idProfesional' value='<?php echo  $hidProfesional;  ?>' />
      <input type='hidden' id='fecha' name='fecha' value='<?php echo  $hidFecha;  ?>' />
      <input type='hidden' id='estado' name='estado' value='Vigente' />
      <input type='hidden' id='IdEmpresa' name='IdEmpresa' value='<?php echo $hidEmpresa1;  ?>' />
    </div>
  </div>



  <?php
    echo form_close();
  ?>

  <?php
 	  if (isset($msg))
    {
      echo "<br>".$msg;
    }
  ?>
  </div>
  </div>
     </div>

   </div>
   <br>
 </div>
<br>

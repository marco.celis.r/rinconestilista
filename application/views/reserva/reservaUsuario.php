<style>
  table {
    border-collapse: collapse;
    width: 100%;
  }

  th, td {
    text-align: left;
    padding: 8px;
  }

  tr:nth-child(even){background-color: #f2f2f2}

  th {
    background-color: #04AA6D;
    color: white;
  }
   #intro {
        background-image: url();
        background-color: #8b9386;
    }

    /* Height for devices larger than 576px */
    @media (min-width: 992px) {
        #intro {
            margin-top: -58.59px;
        }
    }
</style>

<br /><br />
<div id="intro">
   <div class="Container">
     <div class="row justify-content-center">
       <div class="col-xl-5 col-md-8">
         <br>
         <div class="bg-white  rounded-5 shadow-5-strong p-5">
<center><h1>Reservas del usuario <?php echo $this->session->userdata('nombreUsuario'); ?></h1></center>
<?php
  echo form_open_multipart(base_url().'reserva/anularReserva');
  if (isset($msg))
  {
    echo "<br />".$msg;
  }

  if($reservas->num_rows() > 0)
  {
    ?>
    <h4>Cuidado: Si anulas una reserva, esta quedara disponible para que la tome otro usuario</h4>
    <br />
    <table>
      <tr>
        <th>Fecha</th>
        <th>Hora</th>
        <th>Donde</th>
        <th>Profesional</th>
        <th>Servicio</th>
        <th>Anular</th>
      </tr>
    <?php foreach ($reservas->result() as $row)
    {
      $fecha = $row->fecha;
      $hora = $row->hora;
      $nombre = $row->nombre;
      $descripcion = $row->descripcion;
      $idReserva = $row->idReserva;
      $donde = $row->nombreFantasia;

      echo "<tr><td>".$fecha."</td>";
      echo "<td>".$hora."</td>";
      echo "<td>".$donde."</td>";
      echo "<td>".$nombre."</td>";
      echo "<td>".$descripcion."</td>";
      echo "<td><button type='submit' onclick='return validateAnula();' name='action' value='".$idReserva."' class='btn btn-danger btn-xs'> X </button></td></tr>";
    }?>
    </table>
<?php
}
else {
  echo "<h4>No se encontraron reservar</h4>";
}
  echo form_close();
?>
</div>
       </div>
     </div>
   </div>
   <br />
 </div>
<br /><br />

<script type="text/javascript">
function validateAnula()
{
     var r=confirm("Esta accion no se podra deshacer \rEsta seguro de Anular esta reserva?")
    if (r==true)
      return true;
    else
      return false;
}
</script>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="icon" href="<?php echo base_url(); ?>favicon.ico" type="image/gif">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://twitter.github.io/typeahead.js/css/examples.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>
    <script src="https://twitter.github.io/typeahead.js/js/handlebars.js"></script>
    <script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>

    <!-- jQuery UI CSS -->
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">

    <title><?php echo $titulo; ?> | Rincon Estilista</title>

    <style type="text/css">
    #toggle-content{
      display:none;
      }
    #ntipo:checked ~ #toggle-content{
      display:block;
      height:250px;
      }

    </style>

	<!-- pruebas -->
  <!-- iconos importantes -->
 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
  <!-- css -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.css" rel="stylesheet" />
  <!-- MDB -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.js"></script>

  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

  </head>

  <body>

    <div class="container">
    <header class="d-flex flex-wrap justify-content-center py-3 mb-4 border-bottom">
      <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-dark text-decoration-none">
        <svg class="bi me-2" width="40" height="32"><use xlink:href="#bootstrap"/></svg>
        <span class="fs-4">Rincon Estilista</span>
      </a>

      <ul class="nav nav-pills">
        <li class="nav-item"><a href="<?php echo base_url(); ?>" <?php if($activo=="home") { echo " class='nav-link active' aria-current='page'"; } else { echo " class='nav-link' "; } ?> >Home</a></li>

		<!-- servicios general -->
		<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Services</a>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="<?php echo base_url(); ?>peluqueria">Peluqueria</a>
                <a class="dropdown-item" href="<?php echo base_url(); ?>barberia">Barberia</a>
                <a class="dropdown-item" href="<?php echo base_url(); ?>manicure">Manicure</a>
				<a class="dropdown-item" href="<?php echo base_url(); ?>depilacion">Depilacion</a>
              </div>
		</li>
		<!-- fin Servicios general -->


        <li class="nav-item"><a href="<?php echo base_url(); ?>contactos/contactanos" <?php if($activo=="contactos") { echo " class='nav-link active' aria-current='page'"; } else { echo " class='nav-link' "; } ?> >Contactanos</a></li>
        <?php
            $sess_id = $this->session->userdata('nombreUsuario');
            if(empty($sess_id))
            {
                if($activo == "registro")
                { echo "<li class='nav-item'><a href='".base_url()."contactos/agregar/' class='nav-link active' aria-current='page'>Registro</a></li>"; }
                else
                { echo "<li class='nav-item'><a href='".base_url()."contactos/agregar/' class='nav-link'>Registro</a></li>"; }

                if($activo == "login")
                { echo "<li class='nav-item'><a href='".base_url()."login/' class='nav-link active' aria-current='page'>Login</a></li>"; }
                else
                { echo "<li class='nav-item'><a href='".base_url()."login/' class='nav-link'>Login</a></li>"; }

            }
            else
            {
              if (intval($this->session->userdata('tipo')) > 0)
              {
                if($activo=="misReservas")
                { echo "<li class='nav-item'><a href='".base_url()."misReservas/' class='nav-link active' aria-current='page'>Mis Reserva</a></li>"; }
                else
                { echo "<li class='nav-item'><a href='".base_url()."misReservas/' class='nav-link'>Mis Reserva</a></li>"; }

                ?>
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" <?php if($activo=="empresa") { echo " class='nav-link active' aria-current='page'"; } else { echo " class='nav-link' "; } ?> >Empresa</a>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="<?php echo base_url(); ?>profesional/">Profesionales</a>
                      <a class="dropdown-item" href="<?php echo base_url(); ?>servicio/">Servicios</a>
                      <a class="dropdown-item" href="<?php echo base_url(); ?>user/">Agenda</a>
                      <a class="dropdown-item" href="<?php echo base_url(); ?>promocion/">Promociones</a>
                      <a class="dropdown-item" href="<?php echo base_url(); ?>perfil/empresa">Perfil</a>
                    </div>
                  </li>
              <?php
              }
              else
              {
                if($activo=="reserva")
                {
                  echo "<li class='nav-item'><a href='".base_url()."reserva/' class='nav-link active' aria-current='page'>Reserva</a></li>";
                }
                else
                {
                  echo "<li class='nav-item'><a href='".base_url()."reserva/' class='nav-link'>Reserva</a></li>";
                }

                if($activo=="reservaUsuario")
                {?>
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" <?php if($activo=="empresa") { echo " class='nav-link active' aria-current='page'"; } else { echo " class='nav-link' "; } ?> >Perfil</a>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="<?php echo base_url(); ?>reserva/reservaUsuario/">Mis Reservas</a>
                      <a class="dropdown-item" href="<?php echo base_url(); ?>perfil/usuario">Mis Datos</a>
                    </div>
                  </li>
                  <?php
                  //echo "<li class='nav-item'><a href='".base_url()."reserva/reservaUsuario' class='nav-link active' aria-current='page'>Mis Reserva</a></li>";
                }
                else
                {?>
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" <?php if($activo=="empresa") { echo " class='nav-link active' aria-current='page'"; } else { echo " class='nav-link' "; } ?> >Perfil</a>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="<?php echo base_url(); ?>reserva/reservaUsuario/">Mis Reservas</a>
                      <a class="dropdown-item" href="<?php echo base_url(); ?>perfil/usuario">Mis Datos</a>
                    </div>
                  </li>
                  <?php
                  //echo "<li class='nav-item'><a href='".base_url()."reserva/reservaUsuario' class='nav-link'>Mis Reserva</a></li>";
                }
              }

              if($activo=="logout")
              { echo "<li class='nav-item'><a href='".base_url()."logout/' class='nav-link active' aria-current='page'>Logout</a></li>"; }
              else
              { echo "<li class='nav-item'><a href='".base_url()."logout/' class='nav-link'>Logout</a></li>"; }


            }
        ?>
      </ul>

    </header>
  </div>

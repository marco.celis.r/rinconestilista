<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<!-- Script -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- jQuery UI -->
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script type='text/javascript'>
$(document).ready(function(){

   $( "#autouser" ).autocomplete({
    source: function( request, response ) {
     // Fetch data
     $.ajax({
      url: "<?=base_url()?>index.php/User/userList",
      type: 'post',
      dataType: "json",
      data: {
       search: request.term
      },
      success: function( data ) {
       response( data );
      }
     });
    },
    select: function (event, ui) {
     // Set selection
     $('#autouser').val(ui.item.label); // display the selected text
     $('#userid').val(ui.item.value); // save selected id to input
     return false;
    },
    focus: function(event, ui){
       $( "#autouser" ).val( ui.item.label );
       $( "#userid" ).val( ui.item.value );
       return false;
     },
   });


   $( "#autoservice" ).autocomplete({
    source: function( request, response ) {
     // Fetch data
     $.ajax({
      url: "<?=base_url()?>index.php/User/serviceList",
      type: 'post',
      dataType: "json",
      data: {
       search: request.term
      },
      success: function( data ) {
       response( data );
      }
     });
    },
    select: function (event, ui) {
     // Set selection
     $('#autoservice').val(ui.item.label); // display the selected text
     $('#serviceid').val(ui.item.value); // save selected id to input
     return false;
    },
    focus: function(event, ui){
       $( "#autoservice" ).val( ui.item.label );
       $( "#serviceid" ).val( ui.item.value );
       return false;
     },
   });

});
</script>

<!-- Footer -->
<footer class="page-footer font-small unique-color-dark">

    <div style="background-color: #f0f0eb;">
        <div class="container">

            <!-- Grid row-->
            <div class="row py-4 d-flex align-items-center">

                <!-- Grid column -->
                <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
                    <h6 class="mb-0">¡Conéctese con nosotros en las redes sociales!</h6>
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-6 col-lg-7 text-center text-md-right">

                    <!-- Facebook -->
                    <a class="fb-ic">
                       <i class="bi bi-facebook"></i>
                    </a>
					&nbsp;
                    <!-- Twitter -->
                    <a class="tw-ic">
                    <i class="bi bi-twitter"></i>
                    </a>&nbsp;
                    <!-- Google +-->
                    <a class="gplus-ic">
                        <i class="bi bi-google"></i>
                    </a>&nbsp;
                    <!--Linkedin -->
                    <a class="li-ic">
                        <i class="bi bi-linkedin"></i>
                    </a>&nbsp;
                    <!--Instagram-->
                    <a class="ins-ic">
                       <i class="bi bi-instagram"></i>
                    </a>&nbsp;

                </div>
                <!-- Grid column -->

            </div>
            <!-- Grid row-->

        </div>
    </div>

    <!-- Footer Links -->
    <div class="container text-center text-md-left mt-5">

        <!-- Grid row -->
        <div class="row mt-3">

            <!-- Grid column -->
            <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">

                <!-- Content -->
                <h6 class="text-uppercase font-weight-bold">Acerca de</h6>
                
                <p>En Rincon Estilista, ponemos a tu disposición más de 1.000 centros y 13.000 tratamientos de belleza, salud y bienestar</p>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
                <!-- Links -->
                <h6 class="text-uppercase font-weight-bold">Sitios de Interes</h6>
                
                <a href="<?php echo base_url(); ?>peluqueria">Peluqueria</a>
                <br>
                <a href="<?php echo base_url(); ?>barberia">Barberia</a>
                <br>
                <a href="<?php echo base_url(); ?>manicure">Manicure</a>
                <br>
                <a href="<?php echo base_url(); ?>depilacion">Depilacion</a>
                <br>
                <a href="<?php echo base_url(); ?>contactos/contactanos">Contactanos</a>
                <br>
            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
                <!-- Links -->
                <h6 class="text-uppercase font-weight-bold">Rincon Estelista</h6>
                
                <i class="bi bi-map"></i> Av. Vicuña Mackenna 3747
                <br>
                <i class="bi bi-at"></i> info@rinconestilista.com
                <br>
               <i class="bi bi-telephone"></i> +562 22822027
                <br>
               <i class="bi bi-whatsapp"></i> +569 85362301
            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row -->

    </div>
    <!-- Footer Links -->

    <!-- Copyright -->
    <center>&copy;Rincon Estilista 2021</center>

    <!-- Copyright -->

</footer>
<!-- Footer -->
</body>
</html>

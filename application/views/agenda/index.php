<center>
  <h3>Gestion de agenda</h3>
</center>
<br /><br />

<div class="Container">

  <?php echo form_open_multipart(base_url() . 'agenda/agregaragenda'); ?>
  <div class="col-md-12 col-md_offset-4">

    <div class="form-group">
      <label for="nnombre">Nombre</label>
      <input type="text" class="form-control" id="nnombre" name="nnombre" aria-describedby="nnombreHelp" placeholder="Nombre del servicio">

      <small id="nnombreHelp" class="form-text text-muted">Ingrese el nombre o una inicial y presione el boton 'Buscar'.</small>
    </div>

    <input type="submit" class="btn btn-primary" value="Showone" name="goto1btn" />

    <input type="submit" class="btn btn-primary" value="Showtwo" name="goto2btn" />





    <br /><br /><br />
    <button type="submit" class="btn btn-primary" name="guardar">Guardar</button>

    <div class="Container">
      <div class="row">
        <div class="col-md-12">
          <center>
            <?php
            echo "<br /><center>" . $msg . "</center>";
            ?>
          </center>
        </div>
      </div>
    </div>

  </div>


  <br />

  <div class="row">
    <div class="col-4">
      <?php echo validation_errors(); ?>
    </div>
  </div>

  <?php echo form_close(); ?>
</div>
<br />




Search User : <input type="text" id="autouser">

<br><br>
Selected user id : <input type="text" id="userid" value='0'>

<!-- Script -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- jQuery UI -->
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script type='text/javascript'>
  $(document).ready(function() {

    $("#autouser").autocomplete({
      source: function(request, response) {
        // Fetch data
        $.ajax({
          url: "<?= base_url() ?>index.php/User/userList",
          type: 'post',
          dataType: "json",
          data: {
            search: request.term
          },
          success: function(data) {
            response(data);
          }
        });
      },
      select: function(event, ui) {
        // Set selection
        $('#autouser').val(ui.item.label); // display the selected text
        $('#userid').val(ui.item.value); // save selected id to input
        return false;
      },
      focus: function(event, ui) {
        $("#autouser").val(ui.item.label);
        $("#userid").val(ui.item.value);
        return false;
      },
    });

  });
</script>
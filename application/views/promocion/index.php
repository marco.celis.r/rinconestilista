<div id="intro">
  <div class="Container">
    <br>
    <div class="row justify-content-center">
      <div class="col-xl-5 col-md-8">
        <div class="bg-white  rounded-5 shadow-5-strong p-5">
          <center>
            <h3>Promociones</h3>
          </center>
          <br>
          <?php echo form_open_multipart(base_url() . 'promocion/subirImagen'); ?>
          <div class="col-md-12 col-md_offset-4">

            <div class="form-group">
              <label for="nimagen">Imagen de la Promo</label>
              <input type='file' name='imagen' required>
            </div>
            <br>

            <div class="form-group">
              <label for="ndescripcion">Descripcion</label>
              <input type="text" class="form-control" id="ndescripcion" name="ndescripcion" placeholder="Descripcion de la promocion" maxlength="30" minlength="5"  required >
            </div>
            <br>

            <div class="form-group">
              <label for="nvalorante">Valor Anterior</label>
              <input type="number" class="form-control" id="nvalorante" name="nvalorante" placeholder="Valor anterior" min="10" max="199999" maxlength="6" minlength="1"  required >
            </div>
            <br>

            <div class="form-group">
              <label for="nvaloroferta">Valor Oferta</label>
              <input type="number" class="form-control" id="nvaloroferta" name="nvaloroferta" placeholder="Valor oferta" min="10" max="199999" maxlength="6" minlength="1"  required >
            </div>
            <br>

            <div class="Container">
              <div class="row">
                <div class="col-md-12">
                  <center>
                    <?php
                    echo "<br /><center>" . $msg . "</center>";
                    ?>
                  </center>
                </div>
              </div>
            </div>
            <br /><br />
            <button type="submit" value='Enviar' class="btn btn-primary">Guardar</button>
          </div>

          <?php echo form_close(); ?>


          <br />

          <div class="row">
            <div class="col-4">
              <?php echo validation_errors(); ?>
            </div>
          </div>

        </div>
        <br />
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  (function() {
    'use strict'

    var forms = document.querySelectorAll('.needs-validation')

    Array.prototype.slice.call(forms)
      .forEach(function(form) {
        form.addEventListener('submit', function(event) {
          if (!form.checkValidity()) {
            event.preventDefault()
            event.stopPropagation()
          }

          form.classList.add('was-validated')
        }, false)
      })
  })()
</script>

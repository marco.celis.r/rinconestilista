<script type="text/javascript">
  function formatTime(timeInput) {
    intValidNum = timeInput.value;

    if (intValidNum < 24 && intValidNum.length == 2) {
      timeInput.value = timeInput.value + ":";
      return false;
    }
    if (intValidNum == 24 && intValidNum.length == 2) {
      timeInput.value = timeInput.value.length - 2 + "0:";
      return false;
    }
    if (intValidNum > 24 && intValidNum.length == 2) {
      timeInput.value = "";
      return false;
    }

    if (intValidNum.length == 5 && intValidNum.slice(-2) < 60) {
      timeInput.value = timeInput.value + ":";
      return false;
    }
    if (intValidNum.length == 5 && intValidNum.slice(-2) > 60) {
      timeInput.value = timeInput.value.slice(0, 2) + ":";
      return false;
    }
    if (intValidNum.length == 5 && intValidNum.slice(-2) == 60) {
      timeInput.value = timeInput.value.slice(0, 2) + ":00:";
      return false;
    }


    if (intValidNum.length == 8 && intValidNum.slice(-2) > 60) {
      timeInput.value = timeInput.value.slice(0, 5) + ":";
      return false;
    }
    if (intValidNum.length == 8 && intValidNum.slice(-2) == 60) {
      timeInput.value = timeInput.value.slice(0, 5) + ":00";
      return false;
    }
  }


  window.onload = function() {
    var textbox = document.getElementById("frecuencia");
    var maxVal = 180;

    addEvent(textbox, "keyup", function() {
      var thisVal = +this.value;

      this.className = this.className.replace(" input-error ", "");
      if (isNaN(thisVal) || thisVal > maxVal) {
        this.className += " input-error ";
        // Invalid input
      }
    });
  };

  function addEvent(element, event, callback) {
    if (element.addEventListener) {
      element.addEventListener(event, callback, false);
    } else if (element.attachEvent) {
      element.attachEvent("on" + event, callback);
    } else {
      element["on" + event] = callback;
    }
  }
</script>

<style>
  #leftbox {
    float: left;
    width: 25%;
    height: 280px;
  }

  #middlebox {
    float: left;
    width: 50%;
    height: 280px;
  }

  #rightbox {
    float: right;
    width: 25%;
    height: 280px;
  }

  h1 {
    color: green;
    text-align: center;
  }

  input.input-error {
    color: #ff0000;
    border: 1px solid #ff0000;
    box-shadow: 0 0 5px #ff0000;
  }

  #intro {
    background-image: url();

    background-color: #8b9386;
  }

  /* Height for devices larger than 576px */
  @media (min-width: 992px) {
    #intro {
      margin-top: -58.59px;
    }
  }
</style>


<br>
<br>
<div id="intro">
  <div class="Container">
    <?php echo form_open(base_url() . 'user/generarAgenda'); ?>
    <br>
    <div class="row justify-content-center">
      <div class="col-xl-8 col-md-8">
        <div class="bg-white  rounded-5 shadow-5-strong p-5">

          <div id="boxes">
            <h1>Agenda de profesionales</h1>

            <div id="leftbox">
              <img src="<?php echo $this->session->userdata('logo');  ?>" alt="LOGO EMPRESA" />
            </div>

            <div id="middlebox">
              <?php echo $this->session->userdata('descripcion');  ?>
            </div>

            <div id="rightbox">
            </div>

          </div>

          <div class="row">
            <div class="col-4">
              <?php
              echo validation_errors();
              echo $msg;
              ?>
            </div>
          </div>

          <br />
          Profesional : <input type="text" id="autouser" name="autouser" <?php echo isset($autouser) ? 'value="' . $autouser . '" readonly' : ''; ?>> &nbsp; id : <input type="text" id="userid" name="userid" value="<?php echo isset($userid) ? $userid : ''; ?>" readonly>
          <br>
          <br>
          Servicio :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="autoservice" name="autoservice" <?php echo isset($autoservice) ? 'value="' . $autoservice . '" readonly' : ''; ?>> &nbsp; id : <input type="text" id="serviceid" name="serviceid" value="<?php echo isset($serviceid) ? $serviceid : ''; ?>" readonly>
          <br>
          <br>
          <div id="boxes">
            <div id="leftbox">
              Dias de atencion:
            </div>

            <div id="middlebox">
              <input type="checkbox" id="lunes" name="lunes" value="MONDAY" <?php echo isset($lunes) ? $lunes : ''; ?>>
              <label for="lunes"> Lunes</label><br>

              <input type="checkbox" id="martes" name="martes" value="TUESDAY" <?php echo isset($martes) ? $martes : ''; ?>>
              <label for="martes"> Martes</label><br>

              <input type="checkbox" id="miercoles" name="miercoles" value="WEDNESDAY" <?php echo isset($miercoles) ? $miercoles : ''; ?>>
              <label for="miercoles"> Miercoles</label><br>

              <input type="checkbox" id="jueves" name="jueves" value="THURSDAY" <?php echo isset($jueves) ? $jueves : ''; ?>>
              <label for="jueves"> Jueves</label><br>

              <input type="checkbox" id="viernes" name="viernes" value="FRIDAY" <?php echo isset($viernes) ? $viernes : ''; ?>>
              <label for="viernes"> Viernes</label><br>

              <input type="checkbox" id="sabado" name="sabado" value="SATURDAY" <?php echo isset($sabado) ? $sabado : ''; ?>>
              <label for="sabado"> Sabado</label><br>

              <input type="checkbox" id="domingo" name="domingo" value="SUNDAY" <?php echo isset($domingo) ? $domingo : ''; ?>>
              <label for="domingo"> Domingo</label><br>
            </div>

            <div id="rightbox">
            </div>
          </div>

          <label for="appt">Inicio de la jornada</label>
          <input id="horaInicio" name="horaInicio" type="text" placeholder="HH:MM" onkeypress="formatTime(this)" MaxLength="5" <?php echo isset($horaInicio) ? 'value="' . $horaInicio . '" readonly' : ''; ?> />
          <br><br>
          <label for="appt">Termino de la jornada</label>
          <input id="horaFin" name="horaFin" type="text" placeholder="HH:MM" onkeypress="formatTime(this)" MaxLength="5" <?php echo isset($horaFin) ? 'value="' . $horaFin . '" readonly' : ''; ?> />
          <br><br>
          <label for="appt">Frecuencia de atencion:</label>
          <input id="frecuencia" name="frecuencia" type="number" MaxLength="3" placeholder="Cada X minutos" <?php echo isset($frecuencia) ? 'value="' . $frecuencia . '" readonly' : ''; ?> />
          <br><br>

          <?php
          if (isset($agenda)) {
            echo "<br />Puedes seleccionar una o mas Horas para descansar";
            echo "<br />La(s) Hora(s) seleccionada(s) no podra(n) ser reservada(s) por un usuario<br />";
            echo '<br />' . $horas . '<br />';
          }

          ?>

          <div class="col-xl-6 col-md-6">
            
            <tr>
              <td>
                <button type="submit" name="action" value="generar" class="btn btn-primary btn-lg btn-block" <?php echo isset($dias) ? ' disabled' : ''; ?>> Generar Agenda </button>
              </td>
              <td>
                <button type="submit" name="action" value="guardar" class="btn btn-warning btn-lg btn-block" <?php echo isset($guardarAgenda) ? ' enabled' : ' disabled'; ?>> Guardar Agenda </button>
              </td>
            </tr>
          </div>
        </div>
      </div>

    </div>
    <br /><br />
    <input type="hidden" name="dias" id="dias" value="<?php echo isset($dias) ? $dias : ''; ?>" /​>
    <br /><br />
    <?php echo form_close(); ?>

  </div>
</div>
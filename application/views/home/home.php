<style>
    /* Carousel styling */
    #introCarousel,
    .carousel-inner,
    .carousel-item,
    .carousel-item.active {
        height: 100vh;
    }

    .carousel-item:nth-child(1) {
        background-image: url('https://www.flowww.net/hubfs/Sandra/01diciembre/portada-mejores-imagenes-salon-belleza.jpg');
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center center;
    }

    .carousel-item:nth-child(2) {
        background-image: url('https://www.flowww.net/hubfs/Sandra/01diciembre/galeria-mejores-imagenes-salones-belleza-inspiracion-flowww.gif');
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center center;
    }

    .carousel-item:nth-child(3) {
        background-image: url('https://somosnovioschile.com/wp-content/uploads/2018/08/salon-belleza-divas-temuco-DSC03994.jpg');
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center center;
    }

    /* Height for devices larger than 576px */
    @media (min-width: 992px) {
        #introCarousel {
            margin-top: -58.59px;
        }

        #introCarousel,
        .carousel-inner,
        .carousel-item,
        .carousel-item.active {
            height: 70vh;
        }
    }
</style>


<div id="carouselExampleDark" class="carousel carousel-dark slide" data-bs-ride="carousel">
    <div class="carousel-indicators">
        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1" aria-label="Slide 2"></button>
        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 3"></button>
    </div>
    <div class="carousel-inner">
        <div class="carousel-item active" data-bs-interval="10000">

            <div class="carousel-caption d-none d-md-block">
                <div class="bg-white  rounded-2 shadow-2-strong p-2">
                    <h5>ROSTRO Y CUERPO</h5>
                    <p>Belleza a Domicilio entiende lo importante que es el cuidado de la piel, lo idóneo de lucir una piel sana fresca y joven.</p>
                </div>
            </div>
        </div>
        <div class="carousel-item" data-bs-interval="2000">

            <div class="carousel-caption d-none d-md-block">
                <div class="bg-white  rounded-2 shadow-2-strong p-2">
                    <h5>FIESTA Y EVENTOS</h5>
                    <p>Que en tu evento sin importar cuan importante sea, recuerda siempre lucir con estilo, lucir única.</p>
                </div>
            </div>
        </div>
        <div class="carousel-item">

            <div class="carousel-caption d-none d-md-block">
                <div class="bg-white  rounded-2 shadow-2-strong p-2">
                    <h5>CORTE DE CABELLO</h5>
                    <p>Existen muchas de formas de manifestar nuestros estados de ánimo, la más recurrente es intervenir en nuestro pelo.</p>
                </div>
            </div>
        </div>
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
    </button>
</div>

<br>

<!-- Section-->
<section class="py-5">
    <div class="container px-4 px-lg-5 mt-5">
        <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">

          <?php foreach ($promos as $row)
          { ?>

            <div class="col mb-5">
                <div class="card h-100">
                    <!-- Product image-->
                    <img class="card-img-top" src="<?php echo $row->imagen; ?>" alt="..." />
                    <!-- Product details-->
                    <div class="card-body p-4">
                        <div class="text-center">
                            <!-- Product name-->
                            <h5 class="fw-bolder"><?php echo $row->descripcion; ?></h5>
                            <span class="text-muted text-decoration-line-through"><?php echo "$".$row->valorAnterior; ?></span>
                            <?php echo "$".$row->valorPromocion; ?>
                        </div>
                    </div>
                    <?php
                    $ttt = $this->session->userdata('tipo');
                    if($ttt != null)
                    {
                    if($this->session->userdata('tipo') == 0)
                    { ?>
                    <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                        <div class="text-center"><a class="btn btn-outline-dark mt-auto" href="#">Ver mas</a></div>
                    </div>
                  <?php }} ?>
                </div>
            </div>
          <?php } ?>




            <!--<div class="col mb-5">
                <div class="card h-100">
                    <div class="badge bg-dark text-white position-absolute" style="top: 0.5rem; right: 0.5rem">Destacado</div>
                    <img class="card-img-top" src="https://images.jumpseller.com/store/palumbo-store/10006662/tonalizacion.jpeg" alt="..." />
                    <div class="card-body p-4">
                        <div class="text-center">
                            <h5 class="fw-bolder">Color o Tintura</h5>
                            <div class="d-flex justify-content-center small text-warning mb-2">
                                <div class="bi-star-fill"></div>
                                <div class="bi-star-fill"></div>
                                <div class="bi-star-fill"></div>
                                <div class="bi-star-fill"></div>
                                <div class="bi-star-fill"></div>
                            </div>
                            <span class="text-muted text-decoration-line-through">$50.890</span>
                            $43.260
                        </div>
                    </div>
                    <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                        <div class="text-center"><a class="btn btn-outline-dark mt-auto" href="#">Ver mas</a></div>
                    </div>
                </div>
            </div>-->


        </div>
    </div>
</section>

<style>
  #intro {
    background-image: url(https://blog.agendapro.com/hubfs/centro%20de%20belleza%20vac%C3%ADo.png);
    height: 100vh;
  }

  /* Height for devices larger than 576px */
  @media (min-width: 992px) {
    #intro {
      margin-top: -58.59px;
    }
  }
</style>

<br>
<br>
<div id="intro">
  <div class="Container">
    <div class="row justify-content-center">
      <div class="col-xl-5 col-md-8">
        <br>
        <br>
        <div class="bg-white  rounded-5 shadow-5-strong p-5">
          <center>
            <h3>Gracias por visitar</h3>
            <p>El Rincon Estilista</p>
            <br>
            <br>
            <a class="btn btn-primary" href="<?php echo base_url(); ?>" role="button">Volver a la Pagina Principal</a>
            <br>
            <br>
        </div>
      </div>
    </div>
  </div>
</div>
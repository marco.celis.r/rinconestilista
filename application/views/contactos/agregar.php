<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<style>
  #intro {
    background-image: url(https://arlequin.mx/wp-content/uploads/2019/04/autor-desconocido.png);
  }

  /* Height for devices larger than 576px */
  @media (min-width: 100px) {}
</style>


<div id="intro">
  <div class="Container">
    <br>
    <div class="row justify-content-center">
      <div class="col-xl-5 col-md-8">
        <div class="bg-white  rounded-5 shadow-5-strong p-5">
          <center>
            <h3>Registrar</h3>
          </center>
          <br>
          <?php echo form_open_multipart(base_url() . 'contactos/agregarContacto'); ?>
          <div class="col-md-12 col-md_offset-4">
            <div class="form-group">
              <i class="bi bi-person-badge"></i>
              <label for="nnombre">Nombre</label>
              <input type="text" class="form-control" id="nnombre" name="nnombre" aria-describedby="nnombreHelp" placeholder="Ingresa tu nombre" maxlength="50" required>
              <small id="nnombreHelp" class="form-text text-muted" Este dato es obligatorio.</small>
            </div>
            <br>
            <div class="form-group">
              <i class="bi bi-map"></i>
              <label for="ndireccion">Direccion</label>
              <input type="text" class="form-control" id="ndireccion" name="ndireccion" placeholder="Direccion" maxlength="50" required>
            </div><br>
            <div class="form-group">
              <i class="bi bi-telephone"></i>
              <label for="ntelefono">Telefono</label>
              <input type="number" class="form-control" id="ntelefono" name="ntelefono" min="99999999" max="999999999" placeholder="numero de 9 digitos" required onkeyup="textCounter(this,'counter',9);" onblur="ocultar();" onfocus="mostrar()">
            </div><br>
            <div class="form-group">
              <i class="bi bi-key"></i>
              <label for="nclave">Clave</label>
              <input type="password" class="form-control" id="nclave" name="nclave" placeholder="Contraseña" minlength="6" maxlength="20" required>
              <br>
              <input type="password" class="form-control" id="nclave2" name="nclave2" placeholder="Repita la contraseña" minlength="6" maxlength="20" required>
            </div><br>
            <div class="form-group">
              <i class="bi bi-calendar-date"></i>
              <label for="nfecha">Fecha de nacimiento</label>
              <input type="date" step="1" class="form-control" id="nfecha" name="nfecha" placeholder="Fecha de nacimiento" required>
            </div><br>
            <div class="form-group">
              <i class="bi bi-at"></i>
              <label for="ncorreo">Direccion de Correo Electronico</label>
              <input type="mail" class="form-control" id="ncorreo" name="ncorreo" placeholder="tuCorreo@email.com" maxlength="30" required>
            </div>
            <br>
            <div class="form-group">
              <input type="checkbox" id="ntipo" name="ntipo" value="1">
              <label for="ntipo"> Quiero manejar mi agenda en Rincon Estilista</label><br />

              <div id="toggle-content">
                <div class="form-group">
                  <i class="bi bi-shop"></i>
                  <label for="nfantasia">Nombre de Fantasia</label>
                  <input type="text" class="form-control" id="nfantasia" name="nfantasia" placeholder="Nombre de fantasia" maxlength="50">
                </div><br>
                <div class="form-group">
                  <i class="bi bi-card-text"></i>
                  <label for="ncorreo">Descripci&oacute;n</label>
                  <input type="text" class="form-control" id="ndescripcion" name="ndescripcion" placeholder="Pequeña descripcion" maxlength="100">
                </div><br>
                <div class="form-group">
                  <i class="bi bi-map"></i>
                  <label for="ndireccionE">Direccion de la Empresa</label>
                  <input type="text" class="form-control" id="ndireccionE" name="ndireccionE" placeholder="Direccion" maxlength="100">
                </div><br>
                <div class="form-group">
                  <i class="bi bi-telephone"></i>
                  <label for="ndireccionE">Numero telefonico de la Empresa</label>
                  <input type="number" class="form-control" id="ntelefonoE" name="ntelefonoE" placeholder="Fono Empresa" onkeyup="textCounter(this,'counter',9);" onblur="ocultar();" onfocus="mostrar()">
                </div>
                <br />
                <div class="form-group">
                  <i class="bi bi-card-image"></i>
                  <label for="userfile">Logo de tu Empresa</label>
                  <input type="file" accept="image/*" name="userfile" size="60" />
                  <br /><br />
                </div>

              </div>
            </div>
          </div>

          <br /><br /><br /><br /><br /><br /><br />
          <input disabled maxlength="30" size="30" value="10" id="counter" style="display:none;">
          <br>


              <?php
              echo validation_errors();
              if (isset($msg)) {
                echo "<br />" . $msg;
              }
              ?>
           <br>

          <button type="submit" class="btn btn-primary">Guardar</button>





          <?php echo form_close(); ?>

        </div>
      </div>
    </div>
    <br />
  </div>
</div>

<script type="text/javascript">
  (function() {
    'use strict'

    var forms = document.querySelectorAll('.needs-validation')

    Array.prototype.slice.call(forms)
      .forEach(function(form) {
        form.addEventListener('submit', function(event) {
          if (!form.checkValidity()) {
            event.preventDefault()
            event.stopPropagation()
          }

          form.classList.add('was-validated')
        }, false)
      })
  })()



  $("input[type=date]").datepicker({
    dateFormat: 'yy-mm-dd',
    onSelect: function(dateText, inst) {
      $(inst).val(dateText);
    }
  });

  $("input[type=date]").on('click', function() {
    return false;
  });

  function textCounter(field, field2, maxlimit) {
    var countfield = document.getElementById(field2);
    if (field.value.length > maxlimit) {
      field.value = field.value.substring(0, maxlimit);
      return false;
    } else {
      countfield.value = maxlimit - field.value.length + " Digitos";
    }
  }

  function ocultar() {
    document.getElementById("counter").style.display = 'none';
  }

  function mostrar() {
    document.getElementById("counter").style.display = 'block';
  }
</script>

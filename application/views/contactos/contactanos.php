<style>
  #intro {
    background-image: url();

    background-color: #8b9386;
  }

  /* Height for devices larger than 576px */
  @media (min-width: 992px) {
    #intro {
      margin-top: -58.59px;
    }
  }
</style>

<?php echo form_open_multipart(base_url() . 'contactos/enviarContacto'); ?>
<br>
<br>
<div id="intro">
  <div class="Container">
    <br>
    <div class="row justify-content-center">
      <div class="col-xl-6 col-md-8">
        <div class="bg-white  rounded-5 shadow-5-strong p-5">
          <center>
            <h3>Contactanos!</h3>
          </center>
          <br>
          <div class="col-md-11">

            <div class="input-group mb-1">
              <span class="input-group-text" id="basic-addon1">
                <i class="bi bi-person"></i>
              </span>
              <input type="text" id="nnombre" name="nnombre" class="form-control" placeholder="Nombre" aria-label="Nombre" required>


              <span class="input-group-text" id="basic-addon1">
                <i class="bi bi-envelope"></i>
              </span>
              <input type="email" id="nemail" name="nemail" class="form-control" placeholder="Email" aria-label="Email" required>

            </div>
            <br>


            <div class="input-group mb-1">
              <span class="input-group-text" id="basic-addon1">
                <i class="bi bi-telephone"></i>
              </span>
              <input type="number" min="99999999" max="999999999" class="form-control" id="nnumero" name="nnumero" aria-describedby="nemailHelp" placeholder="Numero telefonico" required>

            </div>
            <br>
            <div class="input-group mb-1">
              <span class="input-group-text" id="basic-addon1">
                <i class="bi bi-chat-left-dots"></i>
              </span>
              <input type="text" class="form-control" id="nmensaje" name="nmensaje" aria-describedby="nemailHelp" placeholder="Cual es tu mensaje?" required>

            </div>
            <br>

      <?php echo form_close(); ?>

      <div class="row">
       
          <?php
          echo validation_errors();
          if (isset($msg)) {
            echo "<br />" . $msg;
          }
          ?>
        
      </div>
	  <br />
	  <button class="btn btn-primary" type="submit">Enviar</button>
</div>
        </div>
      </div>
	  
	  
	  
    </div>
    <br />
  </div>
</div>
</div>
</div>

<script type="text/javascript">
  (function() {
    'use strict'

    var forms = document.querySelectorAll('.needs-validation')

    Array.prototype.slice.call(forms)
      .forEach(function(form) {
        form.addEventListener('submit', function(event) {
          if (!form.checkValidity()) {
            event.preventDefault()
            event.stopPropagation()
          }

          form.classList.add('was-validated')
        }, false)
      })
  })()
</script>
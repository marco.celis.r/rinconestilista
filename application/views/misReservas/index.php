<style>
  table {
    border-collapse: collapse;
    width: 100%;
  }

  th, td {
    text-align: left;
    padding: 8px;
  }

  tr:nth-child(even){background-color: #f2f2f2}

  th {
    background-color: #04AA6D;
    color: white;
  }
</style>

<center>
  <div>
    <img src="<?php echo $this->session->userdata('logo'); ?>" alt="Red dot" />
  </div>
  <h3><?php echo $this->session->userdata('nombreFantasia'); ?> - Mis Reservas </h3>
</center>
<br /><br />

<div class="Container">

<?php echo form_open_multipart(base_url().'misReservas/');

if($reservas->num_rows() > 0)
{
  ?>
  <h4>Cuidado: Si anulas una reserva, esta quedara disponible para que la tome otro usuario</h4>
  <br /><br />

  <div class="row">
    <div class="col-4">
      <?php
        echo validation_errors();
        echo $msg;
        ?>
    </div>
  </div>

  <table>
    <tr>
      <th>Usuario</th>
      <th>Fecha</th>
      <th>Profesional</th>
      <th>Servicio</th>
      <th>Estado</th>
      <th>Anula/Recepciona</th>
    </tr>
  <?php foreach ($reservas->result() as $row)
  {
    $fecha = $row->fecha." ".$row->hora;
    $hora = $row->hora;
    $nombre = $row->nombreP;
    $descripcion = $row->descripcion;
    $idReserva = $row->idReserva;
    $nomUsuario = $row->nombreU;
    $estado = $row->estado;
    $dia = $row->fecha;
    $hoy = $row->hoy;
    $fechaIngles = $row->fechaIngles;

    echo "<tr><td>".$nomUsuario."</td>";
    echo "<td>".$fecha."</td>";
    echo "<td>".$nombre."</td>";
    echo "<td>".$descripcion."</td>";
    echo "<td>".$estado."</td>";
    if($estado == 'Recepcionado')
    {
      echo "<td>&nbsp;</td></tr>";
    }
    else
    {
      if($hoy == $fechaIngles)
      {
        echo "<td><button type='submit' onclick='return validateAnula();' name='anula' value='".$idReserva."' class='btn btn-danger btn-xs'> A </button>
                  <button type='submit' onclick='return validateRecepcion();' name='recepciona' value='".$idReserva."' class='btn btn-danger btn-xs'> R </button></td></tr>";
      }
      else
      {
        echo "<td><button type='submit' onclick='return validateAnula();' name='anula' value='".$idReserva."' class='btn btn-danger btn-xs'> A </button></td></tr>";
      }
    }
  }?>
  </table>
<?php
}
else {
echo "<h4>No se encontraron reservar</h4>";
}
echo form_close();
?>

</div>


<script type="text/javascript">
function validateAnula()
{
     var r=confirm("Esta accion no se podra deshacer \rEsta seguro de Anular esta reserva?")
    if (r==true)
      return true;
    else
      return false;
}

function validateRecepcion()
{
     var r=confirm("Esta accion no se podra deshacer \rEsta seguro de Recepcionar esta reserva?")
    if (r==true)
      return true;
    else
      return false;
}
</script>
